@extends('layouts.app')

@section('content')

{{-- =====================Buat Caption================== --}}
<div class="image"> <!-- the image container -->
    <img src="storage/img/asset/Banner Tentang Kami1.jpg" alt="" style="width: 100%;"/> <!-- the image -->
    <h2 class="imageText">
        <span>"Untuk Masa Depan Mereka"</span> <!-- span tag to beautify it efficiently -->
    </h2> <!-- the text -->
</div>

<div style="width: 62.6%; margin:auto; padding-bottom: 4vw;">
    <h1 class="Heading2" style="text-align: center; color: #0E5A89; padding-top: 2vw;">Tentang Kami</h1>

    {{-- ========================Apa itu RangkulMereka?=================== --}}
    <div class="floating-box shadow" style="margin: 2vw 0 4vw 0;">
        <div class="floating-box-content">
            <h3 class="Heading3" style="text-align: center; color: #0E5A89; margin-bottom: 1vw; padding-top: 2vw;">Apa itu RangkulMereka?</h3>
            <p class="Paragraph1" style="text-align: justify; text-align-last: center; padding-bottom: 2vw; line-height: 1.3; margin:0;">Rangkul Mereka merupakan Kampanye Sosial bagi anak-anak Indonesia dengan upaya menghentikan kekerasan pada anak-anak demi masa depan anak dan bangsa yang lebih baik.</p>
        </div>
    </div>

    {{-- =============================Visi & Misi=========================== --}}
    <div class="floating-box shadow" style="margin-top: 4vw;">
        <div class="floating-box-content">

            <h3 class="Heading3" style="text-align: center; color: #0E5A89; margin-bottom: 1vw; padding-top: 2vw;">Visi</h3>
            <p class="Paragraph1" style="text-align: justify; text-align-last: center; line-height: 1.3;">Mewujudkan tatanan sosial yang demokratis, berlandaskan prinsip-prinsip keadilan, dan anti kekerasan terhadap anak-anak usia dibawah 18 tahun yang didasarkan pada UU Perlindungan Anak.</p>

            <h3 class="Heading3" style="text-align: center; color: #0E5A89; margin-bottom: 1vw; padding-top: 2vw;">Misi</h3>
            <p class="Paragraph1" style="text-align: justify; text-align-last: center;padding-bottom: 2vw; line-height: 1.3; margin:0;">1. Membangun awareness tentang kasus kekerasan pada anak yang terjadi di Indonesia.<br>2. Memberikan edukasi dan pemahaman tentang kasus kekerasan pada anak terhadap masyarakat bahwa hal ini yang tidak dapat dianggap sepele dan biasa.<br>3. Merubah pola pikir, gaya didik atau pola asuh masyarakat yang telah dilakukan selama ini melalui program kampanye yang diadakan.<br>4. Mengajak masyarakat untuk berpartisipasi dalam program-program kampanye.</p>

        </div>
    </div>
</div>

@endsection

@section('extendFooter')
<div style="padding: 2vw 0; background-color: #0F75BD;">
    <div style="width: 80.5%; margin: auto;">

        <div style="display: inline-block; width: 48.8%; vertical-align: top;">
            <h2 class="Heading2" style="margin: 1vw 0; color: white;">Ingin Update Terbaru?</h2>
            <p class="Paragraph2" style="margin-bottom: 1.5vw; color: white; width: 60%;">Daftarkan E-mail aktif-mu untuk tahu tentang informasi event, program kampanye dan berita terbaru dari Rangkul Mereka.</p>
            <div style="border-radius: 1.5vw; overflow: hidden; width: 82.5%; height: 3.2vw;">
                <form method="post" action="/email/add" style="height: 100%">
                    @csrf
                    <div class="form-group" style="display: inline-block; width: 66%;height: 100%; vertical-align: top;">
                        <input class="Description2 form-control @error('email') is-invalid @enderror" type="text" name="email" id="email" style="width: 100%; height: 100%; padding: 0 0 0 1.5vw; border-radius: 0; border: 0;" placeholder="Masukan Email Anda*">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div><div style="display: inline-block; width: 34%;height: 100%; vertical-align: top;">
                        <button class="btn myBtn" style="border-radius: unset; width: 100%; height: 100%; margin: 0; vertical-align: top;">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div><div style="display: inline-block; width: 51.2%; vertical-align: top;">
            @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
            <div style="margin-bottom: 0;">
                @else
                <div style="margin-bottom: 2vw;">
                    @endif
                    <div style="display: inline-block; width: 50%; vertical-align: top;">
                        @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                        <h2 class="Heading2" style="margin: 1vw 1vw 1vw 0; color: white; display: inline-block; vertical-align: middle;">Instagram</h2>
                        <a class="AdminBtn AddBtn" style="vertical-align: middle; text-decoration:none" href="/ig/add">
                            <i class="fas fa-plus-square"></i> Add
                        </a>
                        @else
                        <h2 class="Heading2" style="margin: 1vw 0; color: white;">Instagram</h2>
                        @endif
                    </div><div style="display: inline-block; width: 50%; text-align: right;  vertical-align: top;">
                        <a class="btn myBtn shadow" href="">Follow on Instagram</a>
                    </div>
                </div>
                <div class="IGPicContainer">
                    @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                    @for ($i = 0; $i < 4; $i++)
                    <div class="IGDelete" style="text-align: center; margin-bottom: 0.5vw; margin-top: 1vw;">
                        @if($i < $igCount)
                        <a class="AdminBtn DeleteBtn" href="/ig/delete/{{$ig[$i]->id}}">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        <a class="AdminBtn EditBtn" href="/ig/edit/{{$ig[$i]->id}}">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                        @else
                        <a class="AdminBtn DeleteBtn DisabledBtn" href="">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        <a class="AdminBtn EditBtn DisabledBtn" href="">
                            <i class="fas fa-pencil-alt"></i>
                        </a>
                        @endif
                    </div>
                    @endfor
                    @endif

                    {{-- @foreach ($ig as $igData)
                        <div class="IGPic" style="overflow:hidden;">
                            <img src="{{$igData->url}}" style="height: 100%; width: 100%;">
                        </div>
                        @endforeach --}}

                        @for ($i = 0; $i < 4; $i++)
                        <div class="square-image IGPic" style="overflow:hidden;">
                            @if($i < $igCount)
                            <img style="height: 100%; width: 100%;" src="{{$ig[$i]->url}}">
                            @endif
                        </div>
                        @endfor
                    </div>
                </div>

            </div>
        </div>
        @endsection
