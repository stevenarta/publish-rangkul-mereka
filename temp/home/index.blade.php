@extends('layouts.app')

@section('extendedcss')
<link href="{{ asset('css/mobileHome.css') }}" rel="stylesheet">
@endsection

@section('content')
<div id="pcView">
    <div style="background-color:#0f75bd;">
        <div class="container">
            <div style="padding: 7vw 0 0 0;">
                <div style="width: 50%; display: inline-block;">
                    <img src="{{ asset('storage/img/asset/banner.gif') }}" style=" width: 90%;transform: translateY(-7vw) translateX(7vw);">
                </div><div style="width: 50%; display: inline-block; z-index: 20; transform: translateX(5.5vw);">
                    <h1 class="Heading2" style="width: 68%; margin-bottom: 1vw; color: white;">Yuk, Ikut dukung Kami untuk Mengurangi Kekerasan pada Anak.</h1>
                    <a class="btn myBtn shadow" href="/support">Ya, Saya Mendukung</a><br>
                    <a class="btn myBtn shadow" style="margin-top: 0.5vw;" href="/disagree">Tidak Mendukung</a>
                </div>
            </div>
            <div style="text-align: center; padding-bottom: 5vw;">
                <div style="margin-bottom: 1.5vw;">
                    <h1 class="Heading2" style="color: white;">Total Laporan<br>Kasus Kekerasan pada Anak<br>Tahun 2020</h1>
                    <p class="Description1" style="color: white;margin-bottom: 1.5vw; margin-top: 1.5vw;">*Diupdate berkala setiap hari</p>
                </div>
                <div style="height: fit-content;">
                    @for ($i = 0; $i < 6; $i++)<div style="display: inline-block;">
                        @if($i == 0)
                        <div class="myCounter" style="border-top: 0.2vh; border-right: 0.1vh; border-bottom: 0.2vh; border-left: 0.2vh; border-style: solid; border-radius: 1vw 0 0 1vw;">
                            @elseif($i == 5)
                            <div class="myCounter" style="border-top: 0.2vh; border-right: 0.2vh; border-bottom: 0.2vh; border-left: 0.1vh; border-style: solid; border-radius: 0 1vw 1vw 0;">
                                @else
                                <div class="myCounter">
                                    @endif
                                    <h1 style="font-family: futuraMedium;width: fit-content;height: fit-content;margin: auto;font-size: 2.5vw; display:table-cell; vertical-align:middle">
                                        {{$totalKekerasan[$i]}}
                                    </h1>
                                </div>
                            </div>@endfor
                        </div>
                        @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                        <div style="margin-top: 1vw;">
                            <a class="AdminBtn EditBtn" href="/total-kekerasan/edit">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            
            <div  style="background-color: transparent;">
                <div>
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        
                        @if(Auth::user() != null && Auth::user()->hasRole("Admin"))
                        <a class="AdminBtn AddBtn" style="vertical-align: middle; text-decoration:none" href="/carousel/create">
                            <i class="fas fa-plus-square"></i> Add
                        </a>
                        @endif
                        <ol class="carousel-indicators">
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($carousels as $carousel)
                            @if($i == 1)
                            <li data-target="#myCarousel" data-slide-to="{{$i}}" class="indicator active mx-2"></li>
                            @else
                            <li data-target="#myCarousel" data-slide-to="{{$i}}" class="indicator mx-2"></li>
                            @endif
                            @php
                            $i++;
                            @endphp
                            @endforeach
                        </ol>
                        <div class="carousel-inner">
                            @php
                            $i = 0;
                            @endphp
                            @foreach ($carousels as $carousel)
                            @if($i == 0)
                            <a href="/blog/show/{{$carousel->blog_id}}" class="carousel-item active" style="background-image: url({{asset('rangkulmereka/storage/app/public/'.$carousel->img_path)}});">
                                @if(Auth::user() != null && Auth::user()->hasRole("Admin"))
                                <div>
                                    <a class="AdminBtn EditBtn" href="/carousel/edit/{{$carousel->id}}">
                                        <i class="fas fa-pencil-alt"></i> Edit
                                    </a>
                                    <a class="AdminBtn DeleteBtn" href="/carousel/delete/{{$carousel->id}}">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </a>
                                </div>
                                @endif
                                <img class="d-block w-100 h-100" style="height: 100%;" src="{{asset('rangkulmereka/storage/app/public/'.$carousel->img_path)}}">
                            </a>
                            @else
                            <a href="/blog/show/{{$carousel->blog_id}}" class="carousel-item" style="background-image: url({{asset('rangkulmereka/storage/app/public/'.$carousel->img_path)}});">
                                @if(Auth::user() != null && Auth::user()->hasRole("Admin"))
                                <div>
                                    <a class="AdminBtn EditBtn" href="/carousel/edit/{{$carousel->id}}">
                                        <i class="fas fa-pencil-alt"></i> Edit
                                    </a>
                                    <a class="AdminBtn DeleteBtn" href="/carousel/delete/{{$carousel->id}}">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </a>
                                </div>
                                @endif
                                
                                <img class="d-block w-100 h-100" style="height: 100%;" src="{{asset('rangkulmereka/storage/app/public/'.$carousel->img_path)}}">
                            </a>
                            @endif
                            @php
                            $i++;
                            @endphp
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                            <img src="{{ asset('storage/img/asset/LeftIcon.png') }}" style="height: 1.8vw;">
                        </a>
                        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                            <img src="{{ asset('storage/img/asset/RightIcon.png') }}" style="height: 1.8vw;">
                        </a>
                    </div>
                    
                    <div class="myContainer">
                        
                        {{-- =================Apa itu RangkulMereka?===================== --}}
                        <div class="floating-box shadow" style="text-align: center; padding: 3vw 0; margin: 4vw 0;">
                            <div class="floating-box-content">
                                <img src="{{ asset('storage/img/asset/pesawatKertas.png') }}" style="width: 5vw; margin-bottom: 2vh;">
                                <h1 class="Heading2" style="color: #0E5A89;">Apa itu RangkulMereka?</h1>
                                <p class="Paragraph1" style="text-align: justify; text-align-last: center; line-height: 1.2; margin-top: 1.5vw;">Rangkul Mereka merupakan Kampanye Sosial bagi anak-anak Indonesia dengan upaya menghentikan kekerasan pada anak-anak demi masa depan anak dan bangsa yang lebih baik.</p>
                                <a class="btn myBtn shadow"href="/about">Baca Selengkapnya</a>
                            </div>
                        </div>
                        
                        {{-- ============================Berita Terpopular======================= --}}
                        <div class="floating-box shadow">
                            <div style="padding: 2vw 0 0 0; text-align: center;">
                                <h1 class="Heading2" style="color: #0E5A89;">
                                    Berita Terpopular
                                </h1>
                            </div>
                            <div style="width: 94%; margin: auto; padding-bottom: 1.5vw;">
                                @foreach ($mostViewedBlog as $blog)<div style="width: 33.3%; background-color: transparent; margin-top: 1vw; display: inline-block; vertical-align: top;">
                                    <div style="width: 90%; margin-left: auto; margin-right: auto;">
                                        <a href="/blog/show/{{$blog->id}}" style=" text-decoration: none;">
                                            <div class="responsive-image-wide" style="overflow: hidden;">
                                                <img src="{{asset('storage/'.$blog->image)}}" style="height: 100%; width: 100%;">
                                            </div>
                                        </a>
                                        <div class="mt-4">
                                            <a href="/blog/show/{{$blog->id}}" style="text-decoration: none; color: black;">
                                                <h1 class="Title1">{{$blog->title}}</h1>
                                            </a>
                                            <p class="Description1" style="color: #404041;text-align: justify;line-height: 1.5vw; margin-top: 1.5vw;margin-bottom: 0.5;">{{ str_limit($blog->desc, $limit = 200, $end = '...') }}</p>
                                            <p class="Date1" style="color: #404041;">{{ \Carbon\Carbon::parse($blog->created_at)->format('j F, Y') }}</p>
                                        </div>
                                    </div>
                                </div>@endforeach
                            </div>
                        </div>
                        
                        {{-- =============================Ikut Quiz============================= --}}
                        <div class="floating-box shadow" style="text-align: center; padding: 3vw 0; margin-top: 4vw;">
                            <div style="width: 42%; margin:auto;">
                                <img src="{{ asset('storage/img/asset/pesawatKertas.png') }}" style="width: 5vw; margin-bottom: 2vh;">
                                <h1 class="Heading3" style="color: black;">Yuk beri dukungan dengan cara mengikuti quiz ini, apakah Anda pernah melakukan kekerasan pada anak?</h1>
                                <p class="Description1" style="text-align: justify; text-align-last: center; line-height: 1.2; margin-top: 1.5vw; width: 67%; margin:auto;">Hasil quiz ini akan sangat membantu kampanye ini dalam bentuk survey, partisipasi, dan edukasi.</p>
                                @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                                <div>
                                    <a class="btn myBtn shadow" href="/admin/quiz">
                                        Edit Quiz
                                    </a>
                                </div>
                                @else
                                <button type="button" class="btn myBtn shadow" data-toggle="modal" data-target="#QuizModal">
                                    Ikut Quiz
                                </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div style="height: 4vw;"></div>
                </div>
            </div>
        </div>
<div id="mobileView">
    <div style="background-color:#0f75bd;">
        <div>
            <div style="width: fit-content; margin:auto;">
                <img src="{{ asset('storage/img/asset/banner.gif') }}" style=" height: 80vw">
            </div>
            <div style="background-color: #0E5A89; padding: 4vw 0;">
                <h1 class="Heading2" style="width: 28%;margin:auto; margin-bottom: 1vw; color: white; text-align: center;">Yuk, Ikut dukung Kami untuk Mengurangi Kekerasan pada Anak.</h1>
                <div style="width: fit-content; margin: auto;">
                    <a class="btn myBtn shadow" href="/support">Ya, Saya Mendukung</a><br>
                    <a class="btn myBtn shadow" style="margin-top: 0.5vw;" href="/disagree">Tidak Mendukung</a>
                </div>
            </div>
            <div style="text-align: center;background-color: #0E5A89; padding: 4vw 0;">
                <div style="margin-bottom: 1.5vw;">
                    <h1 class="Heading2" style="color: white;">Total Laporan<br>Kasus Kekerasan pada Anak<br>Tahun 2020</h1>
                    <p class="Description1" style="color: white;margin-bottom: 1.5vw; margin-top: 1.5vw;">*Diupdate berkala setiap hari</p>
                </div>
                <div style="height: fit-content;">
                    @for ($i = 0; $i < 6; $i++)<div style="display: inline-block;">
                        @if($i == 0)
                        <div class="myCounter" style="border-top: 0.2vh; border-right: 0.1vh; border-bottom: 0.2vh; border-left: 0.2vh; border-style: solid; border-radius: 1vw 0 0 1vw;">
                            @elseif($i == 5)
                            <div class="myCounter" style="border-top: 0.2vh; border-right: 0.2vh; border-bottom: 0.2vh; border-left: 0.1vh; border-style: solid; border-radius: 0 1vw 1vw 0;">
                                @else
                                <div class="myCounter">
                                    @endif
                                    <h1 style="font-family: futuraMedium;width: fit-content;height: fit-content;margin: auto;font-size: 2.5vw; display:table-cell; vertical-align:middle">
                                        {{$totalKekerasan[$i]}}
                                    </h1>
                                </div>
                            </div>@endfor
                        </div>
                        @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                        <div style="margin-top: 1vw;">
                            <a class="AdminBtn EditBtn" href="/total-kekerasan/edit">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            
            <div  style="background-color: transparent;">
                <div>
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        
                        @if(Auth::user() != null && Auth::user()->hasRole("Admin"))
                        <a class="AdminBtn AddBtn" style="vertical-align: middle; text-decoration:none" href="/carousel/create">
                            <i class="fas fa-plus-square"></i> Add
                        </a>
                        @endif
                        <ol class="carousel-indicators">
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($carousels as $carousel)
                            @if($i == 1)
                            <li data-target="#myCarousel" data-slide-to="{{$i}}" class="indicator active mx-2"></li>
                            @else
                            <li data-target="#myCarousel" data-slide-to="{{$i}}" class="indicator mx-2"></li>
                            @endif
                            @php
                            $i++;
                            @endphp
                            @endforeach
                        </ol>
                        <div class="carousel-inner">
                            @php
                            $i = 0;
                            @endphp
                            @foreach ($carousels as $carousel)
                            @if($i == 0)
                            <a href="/blog/show/{{$carousel->blog_id}}" class="carousel-item active" style="background-image: url({{asset('rangkulmereka/storage/app/public/'.$carousel->img_path)}});">
                                @if(Auth::user() != null && Auth::user()->hasRole("Admin"))
                                <div>
                                    <a class="AdminBtn EditBtn" href="/carousel/edit/{{$carousel->id}}">
                                        <i class="fas fa-pencil-alt"></i> Edit
                                    </a>
                                    <a class="AdminBtn DeleteBtn" href="/carousel/delete/{{$carousel->id}}">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </a>
                                </div>
                                @endif
                                <img class="d-block w-100 h-100" style="height: 100%;" src="{{asset('rangkulmereka/storage/app/public/'.$carousel->img_path)}}">
                            </a>
                            @else
                            <a href="/blog/show/{{$carousel->blog_id}}" class="carousel-item" style="background-image: url({{asset('rangkulmereka/storage/app/public/'.$carousel->img_path)}});">
                                @if(Auth::user() != null && Auth::user()->hasRole("Admin"))
                                <div>
                                    <a class="AdminBtn EditBtn" href="/carousel/edit/{{$carousel->id}}">
                                        <i class="fas fa-pencil-alt"></i> Edit
                                    </a>
                                    <a class="AdminBtn DeleteBtn" href="/carousel/delete/{{$carousel->id}}">
                                        <i class="fas fa-trash-alt"></i> Delete
                                    </a>
                                </div>
                                @endif
                                
                                <img class="d-block w-100 h-100" style="height: 100%;" src="{{asset('rangkulmereka/storage/app/public/'.$carousel->img_path)}}">
                            </a>
                            @endif
                            @php
                            $i++;
                            @endphp
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                            <img src="{{ asset('storage/img/asset/LeftIcon.png') }}" style="height: 1.8vw;">
                        </a>
                        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                            <img src="{{ asset('storage/img/asset/RightIcon.png') }}" style="height: 1.8vw;">
                        </a>
                    </div>
                    
                    <div class="myContainer">
                        
                        {{-- =================Apa itu RangkulMereka?===================== --}}
                        <div class="floating-box shadow" style="text-align: center; padding: 3vw 0; margin: 4vw 0;">
                            <div class="floating-box-content">
                                <img src="{{ asset('storage/img/asset/pesawatKertas.png') }}" style="width: 5vw; margin-bottom: 2vh;">
                                <h1 class="Heading2" style="color: #0E5A89;">Apa itu RangkulMereka?</h1>
                                <p class="Paragraph1" style="text-align: justify; text-align-last: center; line-height: 1.2; margin-top: 1.5vw;">Rangkul Mereka merupakan Kampanye Sosial bagi anak-anak Indonesia dengan upaya menghentikan kekerasan pada anak-anak demi masa depan anak dan bangsa yang lebih baik.</p>
                                <a class="btn myBtn shadow"href="/about">Baca Selengkapnya</a>
                            </div>
                        </div>
                        
                        {{-- ============================Berita Terpopular======================= --}}
                        <div class="floating-box shadow">
                            <div style="padding: 2vw 0 0 0; text-align: center;">
                                <h1 class="Heading2" style="color: #0E5A89;">
                                    Berita Terpopular
                                </h1>
                            </div>
                            <div style="width: 94%; margin: auto; padding-bottom: 1.5vw;">
                                @foreach ($mostViewedBlog as $blog)<div style="background-color: transparent; margin-top: 1vw;vertical-align: top;">
                                    <div style="width: 90%; margin-left: auto; margin-right: auto;">
                                        <a href="/blog/show/{{$blog->id}}" style=" text-decoration: none;">
                                            <div style="overflow: hidden;">
                                                <img src="{{asset('storage/'.$blog->image)}}" style="width: 100%;">
                                            </div>
                                        </a>
                                        <div class="mt-4">
                                            <a href="/blog/show/{{$blog->id}}" style="text-decoration: none; color: black;">
                                                <h1 class="Title1">{{$blog->title}}</h1>
                                            </a>
                                            <p class="Description1" style="color: #404041;text-align: justify;line-height: 1.5vw; margin-top: 1.5vw;margin-bottom: 0.5;">{{ str_limit($blog->desc, $limit = 200, $end = '...') }}</p>
                                            <p class="Date1" style="color: #404041;">{{ \Carbon\Carbon::parse($blog->created_at)->format('j F, Y') }}</p>
                                        </div>
                                    </div>
                                </div>@endforeach
                            </div>
                        </div>
                        
                        {{-- =============================Ikut Quiz============================= --}}
                        <div class="floating-box shadow" style="text-align: center; padding: 3vw 0; margin-top: 4vw;">
                            <div style="width: 42%; margin:auto;">
                                <h1 class="Heading3" style="color: black;">Yuk beri dukungan dengan cara mengikuti quiz ini, apakah Anda pernah melakukan kekerasan pada anak?</h1>
                                <p class="Description1" style="text-align: justify; text-align-last: center; line-height: 1.2; margin-top: 1.5vw; width: 67%; margin:auto;">Hasil quiz ini akan sangat membantu kampanye ini dalam bentuk survey, partisipasi, dan edukasi.</p>
                                @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                                <div>
                                    <a class="btn myBtn shadow" href="/admin/quiz">
                                        Edit Quiz
                                    </a>
                                </div>
                                @else
                                <button type="button" class="btn myBtn shadow" data-toggle="modal" data-target="#QuizModal">
                                    Ikut Quiz
                                </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div style="height: 4vw;"></div>
                </div>
            </div>
        </div>
</div>
@endsection
