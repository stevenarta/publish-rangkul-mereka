<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    {{-- Metadata --}}
    @yield('metadata')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- <meta http-equiv="cache-control" content="no-cache"> --}}

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Rangkul Mereka</title>

    {{-- icons --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
    <script src="../../../../js/app.js" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Chelsea+Market|Nunito&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/util.css') }}" rel="stylesheet">
    @yield('extendedcss')

</head>
<body>
    <div class="loader_bg">
        <img class="loader" src="{{ asset('storage/img/asset/yes.gif') }}">
    </div>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light" id="swup" style="background-color:#0F75BD ;-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);
        -moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);
        box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75); position: fixed; width: 100%; z-index: 20; padding: 0.5vw 0;">
        <div class="container" style="width:82%;max-width: none;">
            <a class="navbar-brand text-white" href="{{ url('/') }}">
                <img src="{{asset('storage/img/asset/Logo_white.png')}}" alt="" style="height:5vw;">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto" style="margin-top: 3.2vw;">
                    <li class="nav-item">
                        @if(request()->path() == "/")
                            <a class="nav-link" href="/" style='color:#FAAE42 ;font-family: "futuraMedium"; font-size: 0.9vw; margin-right: 2vw; margin-left: 2vw;'>Beranda</a>
                        @else
                            <a class="nav-link text-white" href="/" style='font-family: "futuraMedium"; font-size: 0.9vw; margin-right: 2vw; margin-left: 2vw;'>Beranda</a>
                        @endif
                    </li>
                    <li class="nav-item">
                        @if(strstr(request()->path(), "about"))
                            <a class="nav-link" href="/about" style='color:#FAAE42 ;font-family: "futuraMedium"; font-size: 0.9vw; margin-right: 2vw;'>Tentang Kami</a>
                        @else
                            <a class="nav-link text-white" href="/about" style='font-family: "futuraMedium"; font-size: 0.9vw; margin-right: 2vw;'>Tentang Kami</a>
                        @endif
                    </li>
                    <li class="nav-item">
                        @if(strstr(request()->path(), "blog"))
                            <a class="nav-link" href="/blog" style='color:#FAAE42 ;font-family: "futuraMedium"; font-size: 0.9vw; margin-right: 2vw;'>Berita</a>
                        @else
                            <a class="nav-link text-white" href="/blog" style='font-family: "futuraMedium"; font-size: 0.9vw; margin-right: 2vw;'>Berita</a>
                        @endif
                    </li>
                    <li class="nav-item">
                        @if(strstr(request()->path(), "support"))
                        <div>
                            <a class="nav-link myDropDown" href="/support" style='color:#FAAE42 ;font-family: "futuraMedium"; font-size: 0.9vw; margin-right: 2vw;'>Dukung Kami</a>
                            <div class="myDropDownList" style="background-color:#0F75BD;">
                                <a class="nav-link subNav" href="/disagree" style='font-family: "futuraMedium"; font-size: 0.9vw;'>Kenapa Harus Mendukung?</a>
                                {{--  <a class="nav-link subNav" href="/" style='font-family: "futuraMedium"; font-size: 0.9vw;'>Ikut Quiz Kekerasan pada Anak.</a>  --}}
                                @if (Auth::user() != null)
                                <a class="nav-link subNav" href="/quiz" style='font-family: "futuraMedium"; font-size: 0.9vw;'>Ikut Quiz Kekerasan pada Anak.</a>
                                @else
                                <button class="nav-link subNav" type="button" data-toggle="modal" data-target="#QuizModal" style='font-family: "futuraMedium"; font-size: 0.9vw;background-color: transparent; border: 0;'>
                                    Ikut Quiz Kekerasan pada Anak.
                                </button>
                                @endif
                            </div>
                        </div>
                        @else
                        <div>
                            <a class="nav-link text-white myDropDown" href="/support" style='font-family: "futuraMedium"; font-size: 0.9vw; margin-right: 2vw;'>Dukung Kami</a>
                            <div class="myDropDownList" style="background-color:#0F75BD;">
                                <a class="nav-link subNav" href="/disagree" style='font-family: "futuraMedium"; font-size: 0.9vw;'>Kenapa Harus Mendukung?</a>
                                @if (Auth::user() != null)
                                <a class="nav-link subNav" href="/quiz" style='font-family: "futuraMedium"; font-size: 0.9vw;'>Ikut Quiz Kekerasan pada Anak.</a>
                                @else
                                <button class="nav-link subNav" type="button" data-toggle="modal" data-target="#QuizModal" style='font-family: "futuraMedium"; font-size: 0.9vw; background-color: transparent; border: 0;'>
                                    Ikut Quiz Kekerasan pada Anak.
                                </button>
                                @endif
                            </div>
                        </div>
                        @endif
                    </li>

                    {{-- ======================LOGOUT==================== --}}
                    {{-- <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    @if(Auth::user() != null)
                        {{Auth::user()->name}}
                    @endif --}}
                    {{-- ================================================ --}}

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    @if(Auth::user() != null)
                    @if(Auth::user()->hasRole("User"))
                    <img src="{{Auth::user()->img_path}}" style="height: 4vw; width: 4vw; border-radius: 5vw; ">
                    @endif
                    <a class="btn myBtn" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    @endif
                    <!-- Authentication Links -->
                    {{-- @guest --}}
                    {{-- <li class="nav-item">
                        <i class="fas fa-user-circle" style="color: white;font-size: 1.5vw;margin-top: 6px;margin-right: 3px;"></i>
                    </li> --}}
                    {{-- <li class="nav-item">
                        <a class="nav-link text-white" href="{{ route('login') }}" style='font-family: "futuraThin";'>Log In</a>
                    </li> --}}

                    <!-- Button trigger modal -->
                    {{-- <button type="button" class="btn btn-link text-white" data-toggle="modal" data-target="#LoginModal" style='font-family: "futuraThin"; text-decoration:none; font-size: 1vw;'>Log In</button>
                    @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest--}}
            </ul>
        </div>
    </div>
</nav>

<main class="main">
    <div style="background-color: #EFEFEF; position: relative;background: url({{ asset('storage/img/asset/background-illust.png') }}) no-repeat bottom center; background-size: 100% auto;">
    @yield('content')
    </div>
    @yield('extendFooter')
    {{--  ==========================Quiz Modal=====================  --}}
    <div class="modal fade" id="QuizModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white; margin-right: 0vw;text-shadow: unset;">
                        <span aria-hidden="true" style="font-size: 3.5vw;">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="text-align: center;">
                    <h3 class="Heading3" style="color: white;">
                        Lanjutkan Quiz ini dengan Login melalui Akun Anda
                    </h3>
                    <p class="Description2" style="color: white; margin-bottom: 2vw;">
                        *Data Anda Aman Bersama Kami.<br>Kami Tidak Akan Mempublikasikan Informasi Pribadi Anda.
                    </p>
                    {{-- =====Facebook===== --}}
                    <div style="margin-bottom: 1vw;">
                        <a class="btn myBtn shadow" href="{{ url('/login/facebook') }}"  style="margin: auto; background-color: #3b5998; width: 100%; border-width: 0.05vw; border-style:solid; border-color: white; padding: 0.5vw 0;">
                            <img src="{{ asset('storage/img/asset/FacebookIcon.png') }}" style="height: 1.8vw;">
                            <p class="text-white" style="display: inline-block; margin-bottom: 0;">Lanjutkan dengan Facebook</p>
                        </a>
                    </div>


                    {{-- =====Google===== --}}
                    <div>
                        <a class="btn myBtn shadow" href="{{ url('/login/google') }}"  style="margin: auto; background-color: white; width: 100%; padding: 0.5vw 0;">
                            <img src="{{ asset('storage/img/asset/GoogleIcon.png') }}" style="height: 1.8vw;">
                            <p style="display: inline-block; margin-bottom: 0;  color: black;" >Lanjutkan dengan Google</p>
                        </a>
                    </div>


                    <p class="Description1" style="color: white; line-height: 1.4; margin-top: 2vw; text-align: justify; text-align-last: center;">
                        Hasil quiz ini akan sangat membantu kampanye ini dalam bentuk survey, partisipasi dan edukasi.
                    </p>
                </div>
                {{--  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>  --}}
            </div>
        </div>
    </div>
</main>

<footer style="background-color: #0E5A89;-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75); padding: 1.5vw 0;">
    <div class="container"style="width:82%; max-width: none; padding-bottom: 0 !important; margin-bottom: 1vw;">
        <div class="left-side" style="width:33.3%; text-align: left; display: inline-block; vertical-align: top; margin-top: 1.3vw;">
            @if(request()->path() == "/")
                <a class="nav-link" style="color:#FAAE42 ;font-family: 'futuraMedium'; font-size: 0.73vw;" href="/">Beranda</a>
            @else
                <a class="nav-link text-white" style="font-family: 'futuraMedium'; font-size: 0.73vw;" href="/">Beranda</a>
            @endif
            @if(strstr(request()->path(), "about"))
                <a class="nav-link" style="color:#FAAE42 ;font-family: 'futuraMedium'; font-size: 0.73vw;" href="/about">Tentang Kami</a>
            @else
                <a class="nav-link text-white" style="font-family: 'futuraMedium'; font-size: 0.73vw;" href="/about">Tentang Kami</a>
            @endif
            @if(strstr(request()->path(), "blog"))
                <a class="nav-link" style="color:#FAAE42 ;font-family: 'futuraMedium'; font-size: 0.73vw;" href="/blog">Blog</a>
            @else
                <a class="nav-link text-white" style="font-family: 'futuraMedium'; font-size: 0.73vw;" href="/blog">Blog</a>
            @endif
            @if(strstr(request()->path(), "support"))
                <a class="nav-link" style="color:#FAAE42 ;font-family: 'futuraMedium'; font-size: 0.73vw;" href="/support">Dukung Kami</a>
            @else
                <a class="nav-link text-white" style="font-family: 'futuraMedium'; font-size: 0.73vw;" href="/support">Dukung Kami</a>
            @endif
            <p class="text-white" style="font-family: 'futuraMedium'; font-size: 0.72vw; margin-top: 1.3vw; margin-bottom: 0;">&copy; 2020 by RangkulMereka</p>
        </div><div class="middle-side" style="width: 33.3%; text-align: center; display: inline-block;vertical-align: top;">
            <a href="{{ url('/') }}">
                <img src="{{asset('storage/img/asset/Logo_white.png')}}" alt="" style="height:9.8vw;">
            </a>
        </div><div class="right-side" style="width:33.3%; vertical-align: top; display: inline-block;vertical-align: top;margin-top: 1.3vw;">
            <div class="rightWrapper" style="width:fit-content; margin-left:auto;">
                <p class="text-white" style="font-family: 'futuraMedium'; margin-bottom: 0; font-size: 1vw;">Hubungi Kami</p>
                <p class="text-white" style="font-family: 'futuraThin'; margin-bottom: 1.5em; font-size: 1vw;">Email : rangkulmereka@gmail.com</p>
                <div>
                    <a class="text-white" href="/facebook"><i class="fab fa-facebook-f text-white" style="font-size: 1.5vw; margin-right: 2.6vw;"></i></a>
                    <a class="text-white" href="/Instagram"><i class="fab fa-instagram text-white" style="font-size: 1.5vw;"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>

<script src="{{ asset('js/util.js') }}" defer></script>
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5e6d012f10b5e80012afbb67&product=inline-share-buttons" async="async"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
</body>
</html>
