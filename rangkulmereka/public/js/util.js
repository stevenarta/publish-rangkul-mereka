window.addEventListener("load", function(){
    $('.loader_bg').fadeToggle();
    responsiveImage();
    responsiveDiv();
    // setAlign('IGPicContainer', 'IGPic');
    mainPadding();
});

window.addEventListener("resize", function(){
    responsiveImage();
    responsiveDiv();
    // setAlign('IGPicContainer', 'IGPic');
    mainPadding();
});

function activate() {
    var element = document.getElementsByClassName("hamburger");
    var active = document.getElementsByClassName("is-active");

    if(active.length != 0){
        element[0].classList.remove("is-active");
    }else{
        element[0].classList.add("is-active");
    }
}

function mainPadding(){
    var div = document.getElementsByClassName("main");
    var div1 = document.getElementsByClassName("mobileCollapse");

    var header = document.getElementsByClassName("navbar");
    // padding--;

    if(parseInt(header[0].offsetHeight) == 0){
        div[0].style.paddingTop = (parseInt(header[1].offsetHeight)-0.1) + "px";
    }else{
        div[0].style.paddingTop = parseInt(header[0].offsetHeight) + "px";
    }
    
    div1[0].style.marginTop = (parseInt(header[1].offsetHeight)-0.1) + "px";
}

function responsiveImage(){
    var div = document.getElementsByClassName("responsive-image-wide");
    for (var i = 0; i < div.length; i++) {
        var height = parseInt(div[i].offsetWidth) / 1.5;

        div[i].style.height = height+"px";
    }

    var div2 = document.getElementsByClassName("responsive-image-extra-wide");
    for (var i = 0; i < div2.length; i++) {
        var height = parseInt(div2[i].offsetWidth) / 2;

        div2[i].style.height = height+"px";
    }
}

function responsiveDiv(){
    var div3 = document.getElementsByClassName("square-image");
    for (var i = 0; i < div3.length; i++) {
        var height = parseInt(div3[i].offsetWidth);

        div3[i].style.height = height+"px";
    }
}

// function setAlign(parentClass, childCommonClass) {
//     var childDivs = document.getElementsByClassName(childCommonClass);
//     var childDivsTotalWidth = 0;
//     var childDivsLength = childDivs.length;
//     var parentElement = document.getElementsByClassName(parentClass)[0];
//     var parentElementWidth = parentElement.offsetWidth;
//     for (var i = 0; i < childDivsLength; i++) {
//         childDivsTotalWidth += childDivs[i].offsetWidth;
//     }
//     var remainingWidth = parentElementWidth - childDivsTotalWidth;

//     var gap = remainingWidth / (childDivsLength + 1);
//     var leftWidth = gap;
//     for (var j = 0; j < childDivsLength; j++) {
//         if (j > 0) {
//             leftWidth += gap + childDivs[j - 1].offsetWidth;
//         }
//         childDivs[j].style.left = leftWidth + "px";
//     }
// }
