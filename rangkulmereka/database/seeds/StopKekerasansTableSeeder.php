<?php

use App\StopKekerasan;
use Illuminate\Database\Seeder;

class StopKekerasansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'img_path' => 'img/stop_kekerasan_img/item_1.png',
                'title' => 'Bersikap Lebih Bijak',
                'desc' => 'Kalian bisa bertindak lebih bijak dalam menghadapi anak-anak, tidak perlu menggunakan kekerasan fisik dan emosional dan bentuk ancaman. Ajaklah anak-anak berdiskusi atau bujuk mereka tentang hal yang ingin kalian ajarkan.'
            ),
            array(
                'img_path' => 'img/stop_kekerasan_img/item_2.png',
                'title' => 'Pahami Konten dan Media Apa yang diperbolehkan untuk anak-anak',
                'desc' => 'Ketahui konten media apa saja yang dilihat atau dibaca oleh anak-anak. Konten media dapat berbentuk permainan digital, film, situs dan media sosial maupun buku bacaan. Membebaskan anak-anak untuk melihat konten-konten diatas umur mereka bisa memicu cyber crime dan kasus pornografi hingga pelanggaran hukum yang lebih serius.'
            ),
            array(
                'img_path' => 'img/stop_kekerasan_img/item_3.png',
                'title' => 'Hindari pertengkaran rumah tangga dihadapan anak-anak.',
                'desc' => 'Menghindari pertengkaran rumah tangga di hadapan anak-anak adalah salah satu bentuk usaha untuk melindungi anak-anak dari bentuk kekerasan psikologi. Hal ini ternyata sangat berpengaruh pada pola pikir dan prilaku anak-anak di masa depan mereka.'
            ),
            array(
                'img_path' => 'img/stop_kekerasan_img/item_4.png',
                'title' => 'Turut mengantisipasi kekerasan yang terjadi di lingkungan sekitar',
                'desc' => 'Anda bisa turut membantu untuk menghentikan bentuk bully atau kekerasan yang terjadi pada anak-anak di lingkungan sekitarmu. Tentu saja dengan cara positif seperti menegur, meleraikan, atau mengedukasi pelaku tersebut.'
            ),
            array(
                'img_path' => 'img/stop_kekerasan_img/item_5.png',
                'title' => 'Ajak dan ceritakan ke teman-teman anda tentang website edukasi ini.',
                'desc' => 'Mengajak dan ceritakan ke teman-teman anda tentang situs edukasi ini adalah salah satu bentuk partisipasi dukungan untuk mendukung kampanye ini.'
            ),
            array(
                'img_path' => 'img/stop_kekerasan_img/item_6.png',
                'title' => 'Follow RangkulMereka di Sosial Media',
                'desc' => 'Anda tertarik dengan kegiatan dan nasib anak-anak Indonesia? Yuks, ikuti cerita dan kegiatan kami lebih lengkap di akun sosial media kami.'
            )
        );

        StopKekerasan::insert($data);
    }
}
