<?php

use App\Kekerasan;
use Illuminate\Database\Seeder;

class KekerasansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'img_path' => 'img/kekerasan_img/kekerasan_1.png',
                'content' => 'Kekerasan Fisik. seperti contoh tindakan memukul secara fisik, mendorong, menjewer, menampar baik itu hukuman ‘mendidik’ atau bercanda'
            ),
            array(
                'img_path' => 'img/kekerasan_img/kekerasan_2.png',
                'content' => 'Kekerasan Verbal. seperti contoh tindakan memanggil bodoh, membentak, memarahi, membully atau mengolok-ngolok dan sejenisnya.'
            ),
            array(
                'img_path' => 'img/kekerasan_img/kekerasan_3.png',
                'content' => 'Kekerasan Psikis. seperti tindakan mengancam, mengurung atau meninggalkan di ruangan, membiarkan anak-anak membaca atau melihat konten yang tidak ditujukan untuk umurnya mereka.'
            ),
        );

        Kekerasan::insert($data);
    }
}
