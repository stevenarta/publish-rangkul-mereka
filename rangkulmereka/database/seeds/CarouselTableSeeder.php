<?php

use App\Carousel;
use Illuminate\Database\Seeder;

class CarouselTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'img_path' => 'img/carousel_img/carousel_1.jpg',
                'blog_id' => 3
            ),
            array(
                'img_path' => 'img/carousel_img/carousel_2.jpg',
                'blog_id' => 5
            ),
        );

        Carousel::insert($data);
    }
}
