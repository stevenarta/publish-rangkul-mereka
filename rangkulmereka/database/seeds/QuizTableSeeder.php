<?php

use App\Quiz;
use Illuminate\Database\Seeder;

class QuizTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'question' => 'Apakah di rumah anda sering mengajak anak/adik atau kerabat anda berdiskusi tentang apa yang sedang mereka hadapi di sekolah?',
                'option1' => 'Sering',
                'option2' => 'Pernah',
                'option3' => 'Tidak Pernah',
                'point1' => 0,
                'point2' => 5,
                'point3' => 10
            ),
            array(
                'question' => 'Apakah mood anda mempengaruhi cara anda bertindak/berbicara?',
                'option1' => 'Ya',
                'option2' => 'Mungkin',
                'option3' => 'Tidak',
                'point1' => 10,
                'point2' => 5,
                'point3' => 0
            ),
            array(
                'question' => 'Apakah kekerasan merupakan solusi untuk menghukum anak/adik/kerabat anda?',
                'option1' => 'Ya',
                'option2' => 'Mungkin',
                'option3' => 'Tidak',
                'point1' => 10,
                'point2' => 5,
                'point3' => 0
            ),
            array(
                'question' => 'Pernahkah anda melakukan kekerasan fisik pada anak/adik/kerabat anda untuk melampiaskan amarah anda?',
                'option1' => 'Sering',
                'option2' => 'Pernah',
                'option3' => 'Tidak Pernah',
                'point1' => 35,
                'point2' => 15,
                'point3' => 0
            ),
            array(
                'question' => 'Menurut anda apakah tindakan memukul/mencubit/menjewer atau tindakan fisik lainnya satu-satu nya cara mengajari anak yang bandel?',
                'option1' => 'Ya',
                'option2' => 'Mungkin',
                'option3' => 'Tidak',
                'point1' => 10,
                'point2' => 5,
                'point3' => 0
            ),
            array(
                'question' => 'Apakah anda tahu bahwa anak/adik/saudara anda melihat atau membaca sebuah konten yang bukan untuk umur nya mereka?',
                'option1' => 'Ya',
                'option2' => 'Mungkin',
                'option3' => 'Tidak',
                'point1' => 10,
                'point2' => 5,
                'point3' => 0
            ),
            array(
                'question' => 'Apakah anda membimbing anak/adik/ saudara anda untuk melihat atau membaca konten yang hanya untuk umurnya mereka?
                ',
                'option1' => 'Ya',
                'option2' => 'Mungkin',
                'option3' => 'Tidak',
                'point1' => 0,
                'point2' => 5,
                'point3' => 10
            ),
            array(
                'question' => 'Jika Anda melihat sebuah kekerasan pada anak yang terjadi di lingkungan sekitar anda. Apakah Anda bersedia untuk turut melindungi anak tersebut?',
                'option1' => 'Ya',
                'option2' => 'Mungkin',
                'option3' => 'Tidak',
                'point1' => 0,
                'point2' => 5,
                'point3' => 10
            ),
            array(
                'question' => 'Apakah anda tahu bahwa ada Undang-Undang Perlindungan Anak?',
                'option1' => 'Ya',
                'option2' => 'Mungkin',
                'option3' => 'Tidak',
                'point1' => 0,
                'point2' => 5,
                'point3' => 10
            ),
        );

        Quiz::insert($data);
    }
}
