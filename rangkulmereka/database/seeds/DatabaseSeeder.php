<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(BlogsTableSeeder::class);
        $this->call(DampaksTableSeeder::class);
        $this->call(StopKekerasansTableSeeder::class);
        $this->call(InstagramsTableSeeder::class);
        $this->call(KekerasansTableSeeder::class);
        $this->call(LaporanTahunansTableSeeder::class);
        $this->call(RisetUnicefsTableSeeder::class);
        $this->call(TotalKekerasansTableSeeder::class);
        $this->call(QuizTableSeeder::class);
        $this->call(CarouselTableSeeder::class);
    }
}
