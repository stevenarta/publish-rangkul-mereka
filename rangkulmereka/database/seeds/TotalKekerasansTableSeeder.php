<?php

use App\TotalKekerasan;
use Illuminate\Database\Seeder;

class TotalKekerasansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TotalKekerasan::create(
            [
                'total' => 278,
            ]
        );
    }
}
