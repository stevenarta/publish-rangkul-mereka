<?php

use App\RisetUnicef;
use Illuminate\Database\Seeder;

class RisetUnicefsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'img_path' => 'img/unicef_img/riset_1.png',
                'content' => 'Sebanyak 40% anak yang pernah mendapatkan serangan secara fisik paling sedikit satu kali dalam satu tahun.'
            ),
            array(
                'img_path' => 'img/unicef_img/riset_2.png',
                'content' => 'Sebanyak 26% anak yang pernah mendapatkan hukuman berupa fisik dari orang tua dan pengasuh di rumah.'
            ),
            array(
                'img_path' => 'img/unicef_img/riset_3.png',
                'content' => 'Sebanyak 50% anak yang melaporkan mereka di-bully di sekolah.'
            ),
        );

        RisetUnicef::insert($data);
    }
}
