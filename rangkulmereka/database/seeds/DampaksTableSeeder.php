<?php

use App\Dampak;
use Illuminate\Database\Seeder;

class DampaksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'img_path' => 'img/dampak_img/dampak_1.png',
                'title' => 'Bagi Pelaku',
                'desc' => 'Ternyata melakukan kekerasan pun memilki dampak negatif kepada pelaku. Melakukan tindakan kekerasan kepada anak-anak dapat dijerat degan pasal dan undang-undang secara hukum. Bahkan bisa dikenakan denda yang sangat besar dan  berujung ke penjara.'
            ),
            array(
                'img_path' => 'img/dampak_img/dampak_2.png',
                'title' => 'Bagi Korban Perempuan',
                'desc' => 'Selain prilaku merokok dan minum minuman keras. Ternyata ada prilaku menyakiti diri sendiri (self injury), keinginan untuk bunuh diri dan usaha bunuh diri, bahkan penggunaan narkoba.'
            ),
            array(
                'img_path' => 'img/dampak_img/dampak_3.png',
                'title' => 'Bagi Korban Laki-laki',
                'desc' => 'Korban laki-laki cendering berprilaku merokok dan minum minuman keras. Apabila korban sering menerima kekerasan fisik maka tidak menutup kemungkinan bahwa anak ini dapat berprilaku kasar kepada teman-teman bahkan pasangannya.'
            ),
        );

        Dampak::insert($data);
    }
}

