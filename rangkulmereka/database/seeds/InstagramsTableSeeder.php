<?php

use App\Instagram;
use Illuminate\Database\Seeder;

class InstagramsTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $data = array(
            array(
                'url' => 'https://images.unsplash.com/photo-1586462175816-c0e709898f01?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
            ),
            array(
                'url' => 'https://images.unsplash.com/photo-1586455122360-bf852d3d2df7?ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80',
            ),
        );

        Instagram::insert($data);
    }
}
