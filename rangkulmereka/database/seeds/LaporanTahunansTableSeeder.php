<?php

use App\LaporanTahunan;
use Illuminate\Database\Seeder;

class LaporanTahunansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LaporanTahunan::create(
            [
                'img_path' => '/img/laporan_img/laporan.png',
                'content' => 'Sebanyak 13.926 kasus yang terjadi dalam 3 tahun terakhir yaitu 2017-2019 dengan urutan 4 kasus tertinggi, yaitu',
                'list' => '1. Anak berhadapan dengan hukum sebanyak 4,113 kasus
                2. Keluarga dan Pengasuhan alternatif sebanyak 2,437 kasus
                3. Pornografi dan Cyber crime sebanyak 1,942 kasus
                4. Pendidikan sebanyak 1.222 kasus.'
            ],
        );
    }
}
