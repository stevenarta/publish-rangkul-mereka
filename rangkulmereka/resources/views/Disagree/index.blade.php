@extends('layouts.app')

@section('extendedcss')
<link href="{{ asset('css/mobileDisagree.css') }}" rel="stylesheet">
@endsection

@section('content')

{{-- <div class="myGradient" style="position: absolute; top: 0; height: 40vw; width: 100%; background-image: linear-gradient(to bottom, white 80%, rgba(255,255,255,0));"></div> --}}

<div id="pcView">

    {{-- <img src="/img/asset/Banner Dukung Kami.jpg" > --}}
    <div class="image" style="margin-bottom: 4vw;"> <!-- the image container -->
        <img src="/storage/img/asset/Banner Dukung Kami.jpg" alt="" style="width: 100%;"/> <!-- the image -->
        <h2 class="imageText" style="text-align: center;">
            <span>"Dukungan itu semudah memperhatikan Sekitar."</span> <!-- span tag to beautify it efficiently -->
        </h2> <!-- the text -->
    </div>

    <div class="myContainer">
        <h1 class="Heading2" style="text-align: center; color: #0E5A89; margin-bottom: 1.5vw;">Kenapa Harus Mendukung?</h1>



        <div class="floating-box shadow" style="height: 30vw; display: table; width: 100%; margin-bottom: 2vw;">

            <div style="height: fit-content; display: table-cell; vertical-align: middle;">

                <div style="width: 50%;text-align: right; display: inline-block; padding-right: 1.55vw; vertical-align: top;">

                    <div class="floating-box-content-left">

                        <img src="{{asset('rangkulmereka/storage/app/public/'.$laporan->img_path)}}" style="width: 100%;">

                    </div>

                </div><div style="width: 50%;text-align: left; display: inline-block; padding-left: 1.55vw; vertical-align: top;">

                    <div class="floating-box-content-right">

                        @if (Auth::user() != null && Auth::user()->hasRole('Admin'))

                        <h1 class="Heading3" style="color: #0E5A89; margin-bottom: 1.7vw; margin-right: 2vw; display: inline-block; ">1. Jumlah kasus pengaduan anak yang memperihatinkan.</h1>

                        <a class="AdminBtn EditBtn" href="/laporan/edit">

                            <i class="fas fa-pencil-alt"></i>

                        </a>

                        @else

                        <h1 class="Heading3" style="color: #0E5A89; margin-bottom: 1.7vw;">1. Jumlah kasus pengaduan anak yang memperihatinkan.</h1>

                        @endif

                        <h1 class="Heading3" style="color: black; margin-bottom: 1.5vw; text-align: justify;">

                            {{-- {{$laporan->content}} --}}
                            Hasil laporan tahunan KPAI menunjukkan bahwa sebanyak <span style="color:red;">13,926</span> kasus pengaduan anak berdasarkan klaster perlindungan anak yang terjadi di Indonesia dalam kurun waktu 3 tahun terakhir.
                        </h1>
                    </div>

                </div>

            </div>

        </div>



        <div class="floating-box shadow" style="display: table; width: 100%; margin-bottom: 2vw;">

            <div style="height: fit-content; display: table-cell; vertical-align: middle; padding: 2vw 0;">

                <div style="text-align: center;">

                    <h1 class="Heading3" style="color: #0E5A89; margin: 2vw 0 1vw 0;">

                        2. Hasil laporan riset UNICEF kepada anak-anak berusia 13-15 tahun

                    </h1>

                     <p class="Paragraph1" style="line-height: 1.2;">

                        Ditemukan berbagai macam tindakan kekerasan yang dialami anak-anak yaitu:

                    </p>

                    <div style="margin: auto; width: 65%;">

                        @if (Auth::user() != null && Auth::user()->hasRole('Admin'))

                        @php

                            $i = 1;

                        @endphp

                        @foreach ($risets as $riset)<div style="width: 33.3%; display: inline-block; vertical-align: top; margin-bottom: 1vw;">

                            <div style="text-align:center;">

                                <a class="AdminBtn EditBtn" href="/riset/edit/{{$i}}">

                                    <i class="fas fa-pencil-alt"></i>

                                </a>

                            </div>

                            @php

                            $i++;

                            @endphp

                        </div>@endforeach

                        @endif

                        @php

                            $i = 1;

                        @endphp

                        @foreach ($risets as $riset)<div style="width: 33.3%; display: inline-block; vertical-align: top;">

                            <div style="width: fit-content; margin: auto; margin-bottom: 1vw;height: 12vw;">

                                <img  style="height: 100%; margin:auto;" src="{{asset('rangkulmereka/storage/app/public/'.$riset->img_path)}}">

                            </div>

                            <div style="width: 60%; margin: auto;">

                                <div style="width: fit-content; margin: auto;">

                                    <img style="width: 25%;" src="{{ asset('storage/img/asset/number'.$i.'.png') }}">

                                </div>

                                <p class="Paragraph2" style="line-height: 1.2; text-align: justify;">

                                    {{$riset->content}}

                                </p>

                            </div>

                            @php

                            $i++;

                            @endphp

                        </div>@endforeach

                    </div>

                </div>

            </div>

        </div>



        <div class="floating-box shadow" style="height: 26vw; display: table; width: 100%;">

            <div style="height: fit-content; display: table-cell; vertical-align: middle;">

                <div style="width: 50%;text-align: right; display: inline-block;padding-right: 1.55vw;  vertical-align: top;">

                    <div class="floating-box-content-left">

                        <img src="{{ asset('storage/img/asset/art/Rantai Kekerasan.png') }}" style="width: 100%;">

                    </div>

                </div><div style="width: 50%;text-align: left; display: inline-block; padding-left: 1.55vw;  vertical-align: top;">

                    <div class="floating-box-content-right">

                        <h1 class="Heading3" style="color: #0E5A89; line-height: 1.2; margin-bottom: 1vw;">

                            3. Adanya Prinsip Rantai Kekerasan

                        </h1>

                        <p class="Paragraph2" style="line-height: 1.2; text-align: justify;">

                            Faktanya kekerasan yang terjadi juga dapat membuat korban anak melakukan tindakan kekerasan kepada anak-anak lainnya di masa yang akan datang.<br>

                            Yuk, dukung kampanye ini untuk memutuskan rantai ini!


                        </p>

                        <a class="btn myBtn shadow" href="/support">Dukung</a>

                    </div>

                </div>

            </div>

        </div>

        <div style="height: 4vw;"></div>

    </div>
</div>








<div id="mobileView">

    {{-- <img src="/img/asset/Banner Dukung Kami.jpg" > --}}
    <div class="image" style="margin-bottom: 6vw;"> <!-- the image container -->
        <img src="/storage/img/asset/Banner Dukung Kami.jpg" alt="" style="width: 100%;"/> <!-- the image -->
        <h2 class="imageText" style="text-align: center;">
            <span>"Dukungan itu semudah memperhatikan Sekitar."</span> <!-- span tag to beautify it efficiently -->
        </h2> <!-- the text -->
    </div>

    <div class="myContainer">

        <h1 class="Heading2" style="text-align: center; color: #0E5A89; margin-bottom: 6vw;">Kenapa Harus Mendukung?</h1>



        <div class="floating-box shadow" style=" display: table; width: 100%; padding: 6vw 0;  margin-bottom: 4vw">

            <div style="height: fit-content; display: table-cell; vertical-align: middle;">

                <div class="floating-box-content">

                    <div style="height: 45.8vw; width: fit-content; margin: auto; margin-bottom: 8vw;">

                        <img src="{{asset('rangkulmereka/storage/app/public/'.$laporan->img_path)}}" style="height: 100%;">

                    </div>

                    <div>

                        <h1 class="Heading3" style="color: #0E5A89; margin-bottom: 4vw; text-align: justify; text-align-last: center;">1. Jumlah kasus pengaduan anak yang memperihatinkan.</h1>

                        <h1 class="Heading3" style="color: black; margin-bottom: 4vw; text-align: justify; text-align-last: center;">

                            {{-- {{$laporan->content}} --}}
                            Hasil laporan tahunan KPAI menunjukkan bahwa sebanyak <span style="color:red;">13,926</span> kasus pengaduan anak berdasarkan klaster perlindungan anak yang terjadi di Indonesia dalam kurun waktu 3 tahun terakhir.
                        </h1>

                    </div>

                </div>

            </div>

        </div>



        <div class="floating-box shadow" style="margin-bottom: 4vw; padding: 6vw 0;">

            <div class="floating-box-content">

                <div style="text-align: center;">

                    <h1 class="Heading3" style="color: #0E5A89; margin: 2vw 0 1vw 0;">

                        2. Hasil laporan riset UNICEF kepada anak-anak berusia 13-15 tahun

                    </h1>

                    <p class="Paragraph1" style="line-height: 1.2;">

                        Ditemukan berbagai macam tindakan kekerasan yang dialami anak-anak yaitu:

                    </p>

                    <div>

                        @php

                            $i = 1;

                        @endphp

                        @foreach ($risets as $riset)<div>

                            <div style="width: fit-content; margin: auto; margin-bottom: 1vw;height: 45vw;">

                                <img  style="height: 100%; margin:auto;" src="{{asset('rangkulmereka/storage/app/public/'.$riset->img_path)}}">

                            </div>

                            <div>

                                <div style="width: fit-content; margin: auto; margin-bottom: 2vw;">

                                    <img style="width: 25%;" src="{{ asset('storage/img/asset/number'.$i.'.png') }}">

                                </div>

                                <p class="Paragraph2" style="line-height: 1.2; text-align: justify; text-align-last: center;">

                                    {{$riset->content}}

                                </p>

                            </div>

                            @php

                            $i++;

                            @endphp

                        </div>@endforeach

                    </div>

                </div>

            </div>

        </div>



        <div class="floating-box shadow" style="padding: 6vw 0;">

            <div class="floating-box-content">

                <h1 class="Heading3" style="color: #0E5A89; margin-bottom: 4vw; text-align: center;">

                    3. Adanya Prinsip Rantai Kekerasan

                </h1>

                <div style="margin-bottom: 4vw;">

                    <img src="{{ asset('storage/img/asset/art/Rantai Kekerasan.png') }}" style="width: 100%;">

                </div>

                <div>

                    <div>

                        <p class="Paragraph2" style="line-height: 1.2; text-align: justify; text-align-last: center; margin-bottom: 4vw;">

                            Faktanya kekerasan yang terjadi juga dapat membuat korban anak melakukan tindakan kekerasan kepada anak-anak lainnya di masa yang akan datang.<br>

                            Yuk, dukung kampanye ini untuk memutuskan rantai ini!


                        </p>
                        <div style="width: fit-content; margin: auto;">
                            <a class="btn myBtn shadow" href="/support" >Dukung</a>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div style="height: 4vw;"></div>

    </div>
</div>

@endsection

