@extends('layouts.app')

@section('content')
<div class="myContainer" style="padding: 4vw 0;">
    <div class="floating-box shadow">
        <div class="myContainer" style="padding: 4vw 0;">
            <form method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" class="form-control-file @error('image') is-invalid @enderror" name="image" id="image">

                    @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <input type="text" class="form-control @error('content') is-invalid @enderror" name="content" id="content" value="{{$laporan->content}}" placeholder="Content">

                    @error('content')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="list">Description</label>
                    <textarea class="form-control @error('list') is-invalid @enderror" name="list" id="list" rows="10" placeholder="List">{{$laporan->list}}</textarea>

                    @error('list')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
