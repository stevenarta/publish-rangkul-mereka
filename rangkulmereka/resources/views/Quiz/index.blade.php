@extends('layouts.app')

@section('extendedcss')
<link href="{{ asset('css/mobileQuiz.css') }}" rel="stylesheet">
@endsection

@section('content')

<div id="pcView">
    <div class="container"style="width:82%; max-width: none;">
        <div style="margin: 1vw auto;">
            <a href="/" role="button" style="font-size: 1vw; margin-right: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Beranda</a>
            <span style="font-size: 1.5vw; vertical-align: middle;"> > </span>
            <a href="/blog" role="button" style="font-size: 1vw; margin-right: 0.2vw; margin-left: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Dukung Kami</a>
            <span style="font-size: 1.5vw; vertical-align: middle;"> > </span>
            <a href="#" role="button" style="font-size: 1vw; margin-left: 0.2vw; text-decoration: none; color: #0f75bd; font-family: 'futuraMedium'; font-weight: bold; vertical-align: middle;">Ikut Quiz</a>
        </div>
    </div>
</div>

<div id="mobileView">
    <div class="container"style="width:82%; max-width: none; padding-bottom: 4vw;">
        <div style="margin: 4vw auto;">
            <a href="/" role="button" style="font-size: 2.4vw; margin-right: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Beranda</a>
            <span style="font-size: 2.4vw; vertical-align: middle;"> > </span>
            <a href="/blog" role="button" style="font-size: 2.4vw; margin-right: 0.2vw; margin-left: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Dukung Kami</a>
            <span style="font-size: 2.4vw; vertical-align: middle;"> > </span>
            <a href="#" role="button" style="font-size: 2.4vw; margin-left: 0.2vw; text-decoration: none; color: #0f75bd; font-family: 'futuraMedium'; font-weight: bold; vertical-align: middle;">Ikut Quiz</a>
        </div>
    </div>
</div>

<quiz-component></quiz-component>
@endsection
