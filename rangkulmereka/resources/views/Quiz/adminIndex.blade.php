@extends('layouts.app')

@section('content')
<div class="container"style="width:82%; max-width: none; padding: 4vw 0;">
    <div class="floating-box shadow">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col" style="width: 50%">Question</th>
                    <th scope="col">Option 1 : Point</th>
                    <th scope="col">Option 2 : Point</th>
                    <th scope="col">Option 3 : Point</th>
                    <th scope="col">
                        <a class="AdminBtn AddBtn" href="/admin/quiz/add/">
                            <i class="fas fa-plus-square"></i> Add
                        </a>
                    </th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($quizzes as $quiz)
                <tr>
                    <th scope="row">{{$i}}</th>
                    <td>{{$quiz->question}}</td>
                    <td>{{$quiz->option1}} : {{$quiz->point1}}</td>
                    <td>{{$quiz->option2}} : {{$quiz->point2}}</td>
                    <td>{{$quiz->option3}} : {{$quiz->point3}}</td>
                    <td>
                        <a class="AdminBtn EditBtn" href="/admin/quiz/edit/{{$quiz->id}}" style="display: inline-block; margin-right: 1vw;">
                            <i class="fas fa-pencil-alt"></i> Edit
                        </a>
                        <a class="AdminBtn DeleteBtn" href="/admin/quiz/delete/{{$quiz->id}}" style="display: inline-block;">
                            <i class="fas fa-trash-alt"></i> Delete
                        </a>
                    </td>
                </tr>
                @php
                    $i++;
                @endphp
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
