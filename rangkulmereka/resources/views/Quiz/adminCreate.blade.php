@extends('layouts.app')

@section('content')
<div class="myContainer" style="padding: 4vw 0;">
    <div class="floating-box shadow">
        <div class="myContainer" style="padding: 4vw 0;">
            <form method="post">
                @csrf
                <div class="form-group">
                    <label for="question">Question</label>
                    <input type="text" class="form-control @error('question') is-invalid @enderror" name="question" id="question" placeholder="Question">

                    @error('question')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="option1">Option 1</label>
                    <input type="text" class="form-control @error('option1') is-invalid @enderror" name="option1" id="option1" placeholder="Option 1">

                    @error('option1')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="option2">Option 2</label>
                    <input type="text" class="form-control @error('option2') is-invalid @enderror" name="option2" id="option2" placeholder="Option 2">

                    @error('option2')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="option3">Option 3</label>
                    <input type="text" class="form-control @error('option3') is-invalid @enderror" name="option3" id="option3" placeholder="Option 3">

                    @error('option3')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="point1">Point 1</label>
                    <input type="number" class="form-control @error('point1') is-invalid @enderror" name="point1" id="point1" placeholder="Point 1">

                    @error('point1')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="point2">Point 2</label>
                    <input type="number" class="form-control @error('point2') is-invalid @enderror" name="point2" id="point2" placeholder="Point 2">

                    @error('point2')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="point3">Point 3</label>
                    <input type="number" class="form-control @error('point3') is-invalid @enderror" name="point3" id="point3" placeholder="Point 3">

                    @error('point3')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
