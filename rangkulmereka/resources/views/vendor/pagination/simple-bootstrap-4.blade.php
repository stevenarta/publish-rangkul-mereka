
<ul class="pagination" role="navigation" style="background-color: #f0efef;">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
    <li class="disabled" aria-disabled="true" style="background-color: #f0efef;">
        <span class="" style="font-size: 2.5vw; color: grey; margin-right: 10px;"><i class="fas fa-angle-left"></i></span>
    </li>
    @else
    <li class=""style="background-color: #f0efef;">
        <a class="" href="{{ $paginator->previousPageUrl() }}" rel="prev" style="font-size: 2.5vw; color: #0f75bd;margin-right: 10px;"><i class="fas fa-angle-left"></i></a>
    </li>
    @endif
    
    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
    <li class=""style="background-color: #f0efef;">
        <a class="" href="{{ $paginator->nextPageUrl() }}" rel="next" style="font-size: 2.5vw; color: #0f75bd
            ;"><i class="fas fa-angle-right"></i></a>
        </li>
        @else
        <li class="disabled" aria-disabled="true"style="background-color: #f0efef;">
            <span class="" style="font-size: 2.5vw; color: grey
            ;"><i class="fas fa-angle-right"></i></span>
        </li>
        @endif
    </ul>
    
