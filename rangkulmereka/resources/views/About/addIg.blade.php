@extends('layouts.app')

@section('content')
<div class="myContainer" style="padding: 4vw 0;">
    <div class="floating-box shadow">
        <div class="myContainer" style="padding: 4vw 0;">
            <form method="post">
                @csrf
                <div class="form-group">
                    <label for="url">Image Url</label>
                    <input type="text" class="form-control @error('url') is-invalid @enderror" name="url" id="url" placeholder="Url">

                    @error('url')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
