@extends('layouts.app')

@section('extendedcss')
<link href="{{ asset('css/mobileAbout.css') }}" rel="stylesheet">
@endsection

@section('content')

<div id="pcView">
    {{-- =====================Buat Caption================== --}}
    <div class="image"> <!-- the image container -->
        <img src="storage/img/asset/Banner Tentang Kami1.jpg" alt="" style="width: 100%;"/> <!-- the image -->
        <h2 class="imageText">
            <span>“Indonesia Bebas Kekerasan Anak” </span> <!-- span tag to beautify it efficiently -->
        </h2> <!-- the text -->
    </div>

    <div style="width: 62.6%; margin:auto; padding-bottom: 4vw;">
        <h1 class="Heading2" style="text-align: center; color: #0E5A89; padding-top: 2vw;">Tentang Kami</h1>

        {{-- ========================Apa itu RangkulMereka?=================== --}}
        <div class="floating-box shadow" style="margin: 2vw 0 4vw 0;">
            <div class="floating-box-content">
                <h3 class="Heading3" style="text-align: center; color: #0E5A89; margin-bottom: 1vw; padding-top: 2vw;">Apa itu Rangkul Mereka? </h3>
                <p class="Paragraph1" style="text-align: justify; text-align-last: center; padding-bottom: 2vw; line-height: 1.3; margin:0;">Rangkul Mereka merupakan Kampanye Sosial bagi anak-anak Indonesia dengan upaya menghentikan kekerasan pada anak-anak demi masa depan anak dan bangsa yang lebih baik.</p>
            </div>
        </div>

        {{-- =============================Visi & Misi=========================== --}}
        <div class="floating-box shadow" style="margin-top: 4vw;">
            <div class="floating-box-content">

                <h3 class="Heading3" style="text-align: center; color: #0E5A89; margin-bottom: 1vw; padding-top: 2vw;">Visi</h3>
                <p class="Paragraph1" style="text-align: justify; text-align-last: center; line-height: 1.3;">Mewujudkan tatanan sosial yang demokratis, dan mewujudkan Indonesia bebas dari kekerasan pada anak.</p>

                <h3 class="Heading3" style="text-align: center; color: #0E5A89; margin-bottom: 1vw; padding-top: 2vw;">Misi</h3>
                <p class="Paragraph1" style="text-align: justify; text-align-last: center;padding-bottom: 2vw; line-height: 1.3; margin:0;">
                    1. Membangun awareness tentang kasus kekerasan pada anak yang terjadi secara umum di Indonesia.<br>
                    2. Memberikan edukasi dan pemahaman tentang kasus kekerasan pada anak, dampaknya terhadap masyarakat dan aksi untuk menghentikan kekerasan pada anak.<br>
                    3. Menyadarkan dan membangun kepedulian masyarakat kepada anak-anak melalui edukasi dan kampanye yang diadakan.<br>
                    4. Mengajak masyarakat untuk berpartisipasi dalam kampanye.</p>
            </div>
        </div>
    </div>
</div>

<div id="mobileView">
    {{-- =====================Buat Caption================== --}}
    <div class="image"> <!-- the image container -->
        <img src="storage/img/asset/Banner Tentang Kami1.jpg" alt="" style="width: 100%;"/> <!-- the image -->
        <h2 class="imageText">
            <span>“Indonesia Bebas Kekerasan Anak” </span> <!-- span tag to beautify it efficiently -->
        </h2> <!-- the text -->
    </div>

    <div class="myContainer" style="padding-bottom: 4vw;">
        <h1 class="Heading2" style="text-align: center; color: #0E5A89; padding-top: 8vw;">Tentang Kami</h1>

        {{-- ========================Apa itu RangkulMereka?=================== --}}
        <div class="floating-box shadow" style="margin: 4vw 0; padding: 6vw 0;">
            <div class="floating-box-content">
                <h3 class="Heading3" style="text-align: center; color: #0E5A89; margin-bottom: 4vw;">Apa itu Rangkul Mereka?</h3>
                <p class="Paragraph1" style="text-align: justify; text-align-last: center; padding-bottom: 2vw; line-height: 1.3; margin:0;">Rangkul Mereka merupakan Kampanye Sosial bagi anak-anak Indonesia dengan upaya menghentikan kekerasan pada anak-anak demi masa depan anak dan bangsa yang lebih baik.</p>
            </div>
        </div>

        {{-- =============================Visi & Misi=========================== --}}
        <div class="floating-box shadow" style="margin-top: 4vw; padding: 6vw 0;">
            <div class="floating-box-content">

                <div style="padding-bottom: 6vw;">
                    <h3 class="Heading3" style="text-align: center; color: #0E5A89;">Visi</h3>
                    <p class="Paragraph1" style="text-align: justify; text-align-last: center; line-height: 1.3;">Mewujudkan tatanan sosial yang demokratis, dan mewujudkan Indonesia bebas dari kekerasan pada anak.</p>
                </div>

                <h3 class="Heading3" style="text-align: center; color: #0E5A89; margin-bottom: 1vw; padding-top: 2vw;">Misi</h3>
                <p class="Paragraph1" style="text-align: justify; text-align-last: center;padding-bottom: 2vw; line-height: 1.3; margin:0;">
                    1. Membangun awareness tentang kasus kekerasan pada anak yang terjadi secara umum di Indonesia. <br><br>
                    2. Memberikan edukasi dan pemahaman tentang kasus kekerasan pada anak, dampaknya terhadap masyarakat dan aksi untuk menghentikan kekerasan pada anak.<br><br>
                    3. Menyadarkan dan membangun kepedulian masyarakat kepada anak-anak melalui edukasi dan kampanye yang diadakan.<br><br>
                    4. Mengajak masyarakat untuk berpartisipasi dalam kampanye.</p>

            </div>
        </div>
    </div>
</div>
@endsection

@section('extendFooter')
<div id="pcView">
    <div style="padding: 2vw 0; background-color: #0F75BD;">
        <div style="width: 80.5%; margin: auto;">

            <div style="display: inline-block; width: 48.8%; vertical-align: top;">
                <h2 class="Heading2" style="margin: 1vw 0; color: white;">Ingin Update Terbaru?</h2>
                <p class="Paragraph2" style="margin-bottom: 1.5vw; color: white; width: 60%;">Langganan dengan surel kami untuk tahu tentang informasi event, program kampanye dan berita terbaru dari Rangkul Mereka.</p>
                <div style="border-radius: 1.5vw; overflow: hidden; width: 82.5%; height: 3.2vw;">
                    <form method="post" action="/email/add" style="height: 100%">
                        @csrf
                        <div class="form-group" style="display: inline-block; width: 66%;height: 100%; vertical-align: top;">
                            <input class="Description2 form-control @error('email') is-invalid @enderror" type="text" name="email" id="email" style="width: 100%; height: 100%; padding: 0 0 0 1.5vw; border-radius: 0; border: 0;" placeholder="Masukan Email Anda*">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div><div style="display: inline-block; width: 34%;height: 100%; vertical-align: top;">
                            <button class="btn myBtn" style="border-radius: unset; width: 100%; height: 100%; margin: 0; vertical-align: top;">
                                Kirim
                            </button>
                        </div>
                    </form>
                </div>
            </div><div style="display: inline-block; width: 51.2%; vertical-align: top;">
                @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                <div style="margin-bottom: 0;">
                    @else
                    <div style="margin-bottom: 2vw;">
                        @endif
                        <div style="display: inline-block; width: 50%; vertical-align: top;">
                            @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                            <h2 class="Heading2" style="margin: 1vw 1vw 1vw 0; color: white; display: inline-block; vertical-align: middle;">Instagram</h2>
                            <a class="AdminBtn AddBtn" style="vertical-align: middle; text-decoration:none" href="/ig/add">
                                <i class="fas fa-plus-square"></i> Add
                            </a>
                            @else
                            <h2 class="Heading2" style="margin: 1vw 0; color: white;">Instagram</h2>
                            @endif
                        </div><div style="display: inline-block; width: 50%; text-align: right;  vertical-align: top;">
                            <a class="btn myBtn shadow" href="https://www.instagram.com/rangkulmereka/">Ikuti Kami di Instagram</a>
                        </div>
                    </div>
                    <div class="IGPicContainer">
                        @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                        @for ($i = 0; $i < 4; $i++)
                        <div class="IGDelete" style="text-align: center; margin-bottom: 0.5vw; margin-top: 1vw;">
                            @if($i < $igCount)
                            <a class="AdminBtn DeleteBtn" href="/ig/delete/{{$ig[$i]->id}}">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                            <a class="AdminBtn EditBtn" href="/ig/edit/{{$ig[$i]->id}}">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            @else
                            <a class="AdminBtn DeleteBtn DisabledBtn" href="">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                            <a class="AdminBtn EditBtn DisabledBtn" href="">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            @endif
                        </div>
                        @endfor
                        @endif
                        @for ($i = 0; $i < 4; $i++)
                        <div class="square-image IGPic" style="overflow:hidden;">
                            @if($i < $igCount)
                            <img style="height: 100%; width: 100%;" src="{{$ig[$i]->url}}">
                            @endif
                        </div>
                        @endfor
                    </div>
                </div>

            </div>
        </div>
    </div>


<div id="mobileView">
    <div style="padding: 6vw 0; background-color: #0F75BD;">
        <div style="width: 80.5%; margin: auto;">
            <div>
                <h2 class="Heading2" style="margin: 1vw 0; color: white; margin-bottom: 4vw;">Ingin Update Terbaru?</h2>
                <p class="Paragraph2" style="margin-bottom: 4vw; color: white;">Langganan dengan surel kami untuk tahu tentang informasi event, program kampanye dan berita terbaru dari Rangkul Mereka.</p>
                <div style="border-radius: 5vw; overflow: hidden; height: 8.5vw;">
                    <form method="post" action="/email/add" style="height: 100%">
                        @csrf
                        <div class="form-group" style="display: inline-block; width: 66%;height: 100%; vertical-align: top;">
                            <input class="Description2 form-control @error('email') is-invalid @enderror" type="text" name="email" id="email" style="width: 100%; height: 100%; padding: 0 0 0 7.8vw; border-radius: 0; border: 0;" placeholder="Masukan Email Anda*">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div><div style="display: inline-block; width: 34%;height: 100%; vertical-align: top;">
                            <button class="btn myBtn" style="border-radius: unset; width: 100%; height: 100%; margin: 0; vertical-align: top;">
                                Kirim
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div style="height: 0.2vw; width: 100%; background-color: white; margin: 6vw 0;"></div>

            <div>
                <div style="margin-bottom: 4vw;">
                    <div style="display: inline-block; width: 50%; vertical-align: top;">
                        <h2 class="Heading2" style="margin: 1vw 0; color: white;">Instagram</h2>
                    </div><div style="display: inline-block; width: 50%; text-align: right;  vertical-align: top;">
                        <a class="btn myBtn shadow" style="margin:0;" href="https://www.instagram.com/rangkulmereka/">Ikuti Kami di Instagram</a>
                    </div>
                </div>
                <div class="IGPicContainer">
                    @for ($i = 0; $i < 3; $i++)
                    <div class="square-image IGPic" style="overflow:hidden;">
                        @if($i < $igCount)
                        <img style="height: 100%; width: 100%;" src="{{$ig[$i]->url}}">
                        @endif
                    </div>
                    @endfor
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('sponsorSection')
    <div id="pcView">
        <div class="row no-gutters mx-auto" style="width: fit-content; padding: 2vw 0;">
            <div class="col-2" style="margin-right: 1.5vw;">
                <p class="Description1">Disponsori oleh</p>

                <img src="{{ asset('storage/img/asset/sponsor1.png') }}" alt="" style="height: 7vw;">
            </div>
            <div style="width: 0.1vw; background-color: black; opacity: .2;  margin: 0 4vw;"></div>
            <div class="col">
                <p class="Description1">Didukung oleh</p>

                <div class="row no-gutters">
                    <img src="{{ asset('storage/img/asset/KPAI.png') }}" alt="" style="height: 7vw;">
                    <img src="{{ asset('storage/img/asset/TutWuriHandayani.png') }}" alt="" style="height: 7vw; margin: 0 4vw;">
                    <img src="{{ asset('storage/img/asset/SahabarKeluarga.png') }}" alt="" style="height: 7vw;">
                </div>
            </div>
        </div>
    </div>

    <div id="mobileView">
        <div class="col text-center" style="padding: 4vw 0;">
            <div class="row no-gutters mx-auto">
                <div class="col">
                    <p class="Description1">Disponsori oleh</p>

                    <img src="{{ asset('storage/img/asset/sponsor1.png') }}" alt="" style="height: 15vw;">
                </div>
            </div>
            <div class="row no-gutters mx-auto" style="width: fit-content;">
                <div class="col">
                    <div class="col" style="height: 0.2vw; background-color: black; opacity: .2; margin: 4vw 0;"></div>
                    <div class="col">
                        <p class="Description1">Didukung oleh</p>

                        <div class="row no-gutters">
                            <img src="{{ asset('storage/img/asset/KPAI.png') }}" alt="" style="height: 15vw;">
                            <img src="{{ asset('storage/img/asset/TutWuriHandayani.png') }}" alt="" style="height: 15vw; margin: 0 4vw;">
                            <img src="{{ asset('storage/img/asset/SahabarKeluarga.png') }}" alt="" style="height: 15vw;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
