@extends('layouts.app')

@section('extendedcss')
<link href="{{ asset('css/mobileBlog.css') }}" rel="stylesheet">
@endsection

@section('content')

<div id="pcView">
    <div class="myContainer" style="background-color: transparent; margin-top: 3vw;">

        <div class="floating-box shadow">

            <div style="padding: 2vw 0 0 0; text-align: center;">

                <h1 class="Heading2" style="color: #0E5A89;">

                    Berita Terpopular

                </h1>

            </div>

            <div style="width: 94%; margin: auto; padding-bottom: 1.5vw;">

                @foreach ($mostViewedBlog as $blog)<div style="width: 33.3%; background-color: transparent; margin-top: 1vw; display: inline-block; vertical-align: top;">

                    <div style="width: 90%; margin-left: auto; margin-right: auto;">

                    <a href="/blog/show/{{$blog->id}}" style=" text-decoration: none;">

                            <div class="responsive-image-wide" style="overflow: hidden;">

                                <img src="{{asset('rangkulmereka/storage/app/public/'.$blog->image)}}" style="height: 100%; width: 100%;">

                            </div>

                        </a>

                        <div style="margin-top: 1vw;">

                            <a href="/blog/show/{{$blog->id}}" style="text-decoration: none; color: black;">

                                <h1 class="Title1">{{$blog->title}}</h1>

                            </a>

                            <p class="Description1" style="color: #404041;text-align: justify;line-height: 1.5vw; margin-top: 1.5vw;margin-bottom: 0.5;">{{ str_limit($blog->desc, $limit = 200, $end = '...') }}</p>

                            <p class="Date1" style="color: #404041;">{{ \Carbon\Carbon::parse($blog->created_at)->format('j F, Y') }}</p>

                        </div>

                    </div>

                </div>@endforeach

            </div>

        </div>



        <div class="floating-box shadow" style="margin-top: 4vw;">

            <div style="width: 90%;margin: auto;padding: 2vw 0 0.3vw 0;">

                @if (Auth::user() != null && Auth::user()->hasRole('Admin'))

                <div>

                    <div style="display: inline-block; width: 50%;">

                        <h1 class="Heading2" style="color: #0E5A89; margin:0; margin-left: 0.2vw;">

                            Berita

                        </h1>

                    </div><div style="display: inline-block; width: 50%; text-align:right;">

                        <a class="AdminBtn AddBtn" href="/blog/create"><i class="fas fa-plus-square"></i> Add</a>

                    </div>

                </div>

                @else

                <h1 class="Heading2" style="color: #0E5A89; margin:0; margin-left: 0.2vw;">

                    Berita

                </h1>

                @endif

            </div>

            <div style="width: 90%; height: 88%; margin: auto;">

                <blog-pagination></blog-pagination>

            </div>

        </div>
        <div style="height: 4vw"></div>
    </div>
</div>

<div id="mobileView">
    <div class="myContainer" style="background-color: transparent; margin-top: 3vw;">

        {{-- ============================Berita Terpopular======================= --}}
        <div class="floating-box shadow" style="padding: 6vw 0;">
            <div class="floating-box-content">
                <div style="margin-bottom: 6vw; text-align: center;">
                    <h1 class="Heading2" style="color: #0E5A89;">
                        Berita Terpopular
                    </h1>
                </div>
                <div style="padding-bottom: 1.5vw;">
                    @foreach ($mostViewedBlog as $blog)<div style="background-color: transparent; margin-top: 2vw;vertical-align: top;">
                        <div>
                            <a href="/blog/show/{{$blog->id}}" style=" text-decoration: none;">
                                <div style="overflow: hidden;">
                                    <img src="{{asset('storage/'.$blog->image)}}" style="width: 100%;">
                                </div>
                            </a>
                            <div class="mt-4">
                                <a href="/blog/show/{{$blog->id}}" style="text-decoration: none; color: black;">
                                    <h1 class="Title1">{{$blog->title}}</h1>
                                </a>
                                <p class="Description1" style="color: #404041;text-align: justify;line-height: 1.2; margin-top: 1.5vw;margin-bottom: 0.5;">{{ str_limit($blog->desc, $limit = 200, $end = '...') }}</p>
                                <p class="Date1" style="color: #404041;">{{ \Carbon\Carbon::parse($blog->created_at)->format('j F, Y') }}</p>
                            </div>
                        </div>
                    </div>@endforeach
                </div>
            </div>
        </div>

        {{-- =======================Blog Pagination================== --}}
        <div class="floating-box shadow" style="margin-top: 4vw; padding: 6vw 0;">
            <div class="floating-box-content">
                <div style="padding: 2vw 0 0.3vw 0;">
                    <h1 class="Heading2" style="color: #0E5A89;">
                        Berita
                    </h1>
                </div>
                <div>
                    <blog-pagination></blog-pagination>
                </div>
            </div>
        </div>
        <div style="height: 4vw"></div>
    </div>
</div>

@endsection

