@extends('layouts.app')

@section('content')
<div class="myContainer" style="padding: 4vw 0;">
    <div class="floating-box shadow">
        <div class="myContainer" style="padding: 4vw 0;">
            <form method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="title" placeholder="Title" value="{{$blog->title}}">

                    @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Subtitle</label>
                    <input type="text" class="form-control @error('subtitle') is-invalid @enderror" name="subtitle" id="subtitle" placeholder="Subtitle" value="{{$blog->subtitle}}">

                    @error('subtitle')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="category">Category</label>
                    <select class="form-control @error('category') is-invalid @enderror" name="category" id="category">
                        @if($blog->category == "Artikel")
                        <option value="Artikel" selected>Artikel</option>
                        <option value="Aksi">Aksi</option>
                        @elseif($blog->category == "Aksi")
                        <option value="Artikel">Artikel</option>
                        <option value="Aksi" selected>Aksi</option>
                        @endif
                    </select>

                    @error('category')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="url">Url</label>
                    <input type="text" class="form-control @error('url') is-invalid @enderror" name="url" id="url" placeholder="Url" value="{{$blog->url}}">

                    @error('url')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="postedBy">Posted by</label>
                    <input type="text" class="form-control @error('postedBy') is-invalid @enderror" name="postedBy" id="postedBy" placeholder="Posted By" value="{{$blog->postedBy}}">

                    @error('postedBy')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="desc">Description</label>
                    <textarea class="form-control @error('desc') is-invalid @enderror" name="desc" id="desc" rows="3" placeholder="Description">{{$blog->desc}}</textarea>

                    @error('desc')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" class="form-control-file @error('image') is-invalid @enderror" name="image" id="image">

                    @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
