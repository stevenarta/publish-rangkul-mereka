@extends('layouts.app')

@section('extendedcss')
<link href="{{ asset('css/mobileBlog.css') }}" rel="stylesheet">
@endsection

@section('metadata')
    <meta property="og:image:secure_url" itemprop="image" content="{{ asset('rangkulmereka/storage/app/public/'.$blog->image ) }}" />
    <meta property="og:image" itemprop="image" content="{{ asset('rangkulmereka/storage/app/public/'.$blog->image ) }}" />
    <meta property="og:title" content="{{$blog->title}}" />
    <meta property="og:description" content="{{ str_limit($blog->desc, $limit = 100, $end = '...') }}" />
    <meta property="og:type"  content="website" />
    <meta property="og:image:width" content="256">
    <meta property="og:image:height" content="256">
@endsection
@section('content')
<div id="pcView">
    <div class="container"style="width:82%; max-width: none;">
        <div style="margin: 1vw auto 1vw auto;">
            <a href="/" role="button" style="font-size: 1vw; margin-right: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Beranda</a>
            <span style="font-size: 1.5vw; vertical-align: middle;"> > </span>
            <a href="/blog" role="button" style="font-size: 1vw; margin-right: 0.2vw; margin-left: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Berita</a>
            <span style="font-size: 1.5vw; vertical-align: middle;"> > </span>
            <a href="#" role="button" style="font-size: 1vw; margin-left: 0.2vw; text-decoration: none; color: #0f75bd; font-family: 'futuraMedium'; font-weight: bold; vertical-align: middle;">{{$blog->category}}</a>
        </div>

        <div style="vertical-align: top; padding-bottom: 4vw;">
            <div style="width: 63%; display: inline-block;">
                <div class="floating-box shadow" style="width: 97.7%; margin-right: auto;">
                    <div style="width: 70%; margin: auto;">
                        <h1 class="Heading2" style="color: #0E5A89; padding: 2vw 0 0 0;">{{$blog->title}}</h1>
                        <p class="Date1" style="color: #404041;">Ditulis oleh {{$blog->postedBy}}. {{ \Carbon\Carbon::parse($blog->created_at)->format('j F, Y') }}.</p>
                        <div class="responsive-image-wide">
                            <a href="{{ asset('rangkulmereka/storage/app/public/'.$blog->image ) }}" data-lightbox="image-pc">
                                <img src="{{ asset('rangkulmereka/storage/app/public/'.$blog->image ) }}" style="width: 100%;height: 100%;">
                            </a>
                        </div>
                        <div style="padding: 3vw 0 1.5vw 0;">
                            <p class="Paragraph1" style="text-align: justify; text-justify: inter-word; color: #404041;">{!! nl2br(e($blog->desc)) !!}</p>
                            <p class="WebLink">Sumber: <a href="{{$blog->url}}" style="color: orange; text-decoration: none;">{{$blog->url}}</a></p>
                            <div>
                                <p class="Heading3" style="display: inline-block;vertical-align: middle; color: #0E5A89;">Bagikan</p>

                                <div style="display: inline-block; height: 3.5vw; width: 3.5vw; position: relative;vertical-align: middle; margin-left:1.5vw;">
                                    <div style="display: table; margin: auto;">
                                        <div data-network="facebook" id="shareBtn" class="st-custom-button facebookButton shadow" style="display: table-cell; vertical-align: middle;">
                                            <i class="fab fa-facebook-f socialLogo" style="display: block; margin: auto;"></i>
                                        </div>
                                    </div>
                                </div>

                                <div style="display: inline-block; height: 3.5vw; width: 3.5vw;vertical-align: middle;">
                                    <div style="display: table; margin: auto;">
                                        <div data-network="whatsapp" id="shareBtn" class="shareBtn st-custom-button whatsAppButton shadow" style="display: table-cell; vertical-align: middle;" data-url="https://rangkulmereka.asibu.com/blog/show/{{$blog->id}}">
                                            <i class="fab fa-whatsapp socialLogo" style="display: block; margin: auto;"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><div style="width: 37%; display: inline-block; vertical-align: top;">
                <div class="floating-box shadow" style="width: 97%; margin-left: auto; padding-bottom: 1vw;">
                    <div style="width: 90%; margin: auto; margin-bottom: 1vw;">
                        <h1 class="Heading2" style="padding: 2vw 0 1vw 0;color: #0E5A89;">Berita Lainnya</h1>
                        <div class="scroll">
                            @foreach ($anotherBlog as $blogs)
                            <div style="padding-bottom: 2vw; background-color: transparent; margin-top: 0.7vw; display: inline-block;">
                                <div style=" width: 100%; display:block; margin:auto; vertical-align: top;">
                                    <div style="width: 50%; display: inline-block; vertical-align: top; margin: auto;">
                                        <div style="width: 95%; margin: auto;">
                                            <a href="/blog/show/{{$blogs->id}}" style="text-decoration: none;">
                                                <div class="responsive-image-wide" style="overflow: hidden;">
                                                    <img src="{{ asset('rangkulmereka/storage/app/public/'.$blogs->image ) }}" style="width: 100%; height: 100%">
                                                </div>
                                            </a>
                                        </div>
                                    </div><div style="width: 50%; display: inline-block; margin: auto;">
                                        <div style="width: 95%; margin: auto;">
                                            <a href="/blog/show/{{$blogs->id}}" style="text-decoration: none; color: black;">
                                                <h1 class="Title2" style="margin-bottom: 0.2vw;">{{ $blogs->title }}</h1>
                                            </a>
                                            <p class="Description2" style="margin-bottom: 0.5vw;text-align: justify;text-justify: inter-word; color: #404041; line-height:1.2; ">{{ str_limit($blogs->desc, $limit = 100, $end = '...') }}</p>
                                            <p class="Date2" style="color: #404041;margin: 0;">{{ \Carbon\Carbon::parse($blog->created_at)->format('j F, Y') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<div id="mobileView">
    <div class="myContainer">
        <div style="margin: 4vw auto;">
            <a href="/" role="button" style="font-size: 2.4vw; margin-right: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Beranda</a>
            <span style="font-size: 2.4vw; vertical-align: middle;"> > </span>
            <a href="/blog" role="button" style="font-size: 2.4vw; margin-right: 0.2vw; margin-left: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Berita</a>
            <span style="font-size: 2.4vw; vertical-align: middle;"> > </span>
            <a href="#" role="button" style="font-size: 2.4vw; margin-left: 0.2vw; text-decoration: none; color: #0f75bd; font-family: 'futuraMedium'; font-weight: bold; vertical-align: middle;">{{$blog->category}}</a>
        </div>

        <div style="vertical-align: top; padding-bottom: 4vw;">
            <div class="floating-box shadow" style="padding: 6vw 0; margin-bottom: 4vw;">
                <div class="floating-box-content">
                    <h1 class="Heading2" style="color: #0E5A89; padding: 2vw 0 0 0;">{{$blog->title}}</h1>
                    <p class="Date1" style="color: #404041;">Ditulis oleh {{$blog->postedBy}}. {{ \Carbon\Carbon::parse($blog->created_at)->format('j F, Y') }}.</p>
                </div>
                <div class="responsive-image-wide">
                    <a href="{{ asset('rangkulmereka/storage/app/public/'.$blog->image ) }}" data-lightbox="image-mobile">
                        <img src="{{ asset('rangkulmereka/storage/app/public/'.$blog->image ) }}" style="width: 100%;height: 100%;">
                    </a>
                </div>
                <div class="floating-box-content">
                    <div style="padding: 3vw 0 1.5vw 0;">
                        <p class="Paragraph1" style="text-align: justify; text-justify: inter-word; color: #404041; line-height: 1.2;">{!! nl2br(e($blog->desc)) !!}</p>
                        <p class="WebLink">Sumber: <a href="{{$blog->url}}" style="color: orange; text-decoration: none;">{{$blog->url}}</a></p>
                        <div>
                            <p class="Heading3" style="display: inline-block;vertical-align: middle; color: #0E5A89;">Bagikan</p>

                            <div style="display: inline-block; position: relative;vertical-align: middle; margin-left:1.5vw;">
                                <div style="display: table; margin: auto;">
                                    <div data-network="facebook" id="shareBtn" class="st-custom-button facebookButton shadow" style="display: table-cell; vertical-align: middle;">
                                        <i class="fab fa-facebook-f socialLogo" style="display: block; margin: auto;"></i>
                                    </div>
                                </div>
                            </div>

                            <div style="display: inline-block; vertical-align: middle;  margin-left:1.5vw;">
                                <div style="display: table; margin: auto;">
                                    <div data-network="whatsapp" id="shareBtn" class="shareBtn st-custom-button whatsAppButton shadow" style="display: table-cell; vertical-align: middle;" data-url="https://rangkulmereka.asibu.com/blog/show/{{$blog->id}}">
                                        <i class="fab fa-whatsapp socialLogo" style="display: block; margin: auto;"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="floating-box shadow" style="padding: 6vw 0;">
                    <div style="width: 90%; margin: auto; margin-bottom: 1vw;">
                        <h1 class="Heading2" style="padding-bottom: 2vw; color: #0E5A89;">
                            Berita Lainnya
                        </h1>

                        <div class="scroll">
                            @foreach ($anotherBlog as $blogs)
                            <div style="padding-bottom: 2vw; background-color: transparent; margin-top: 0.7vw; display: inline-block;">
                                <div style=" width: 100%; display:block; margin:auto; vertical-align: top;">
                                    <div style="width: 50%; display: inline-block; vertical-align: top; margin: auto;">
                                        <div style="width: 95%; margin: auto;">
                                            <a href="/blog/show/{{$blogs->id}}" style="text-decoration: none;">
                                                <div class="responsive-image-wide" style="overflow: hidden;">
                                                    <img src="{{ asset('rangkulmereka/storage/app/public/'.$blogs->image ) }}" style="width: 100%; height: 100%">
                                                </div>
                                            </a>
                                        </div>
                                    </div><div style="width: 50%; display: inline-block; margin: auto;">
                                        <div style="width: 95%; margin: auto;">
                                            <a href="/blog/show/{{$blogs->id}}" style="text-decoration: none; color: black;">
                                                <h1 class="Title2" style="margin-bottom: 0.2vw;">{{ $blogs->title }}</h1>
                                            </a>
                                            <p class="Description2" style="margin-bottom: 0.5vw;text-align: justify;text-justify: inter-word; color: #404041; line-height:1.2; ">{{ str_limit($blogs->desc, $limit = 150, $end = '...') }}</p>
                                            <p class="Date2" style="color: #404041;margin: 0;">{{ \Carbon\Carbon::parse($blog->created_at)->format('j F, Y') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
