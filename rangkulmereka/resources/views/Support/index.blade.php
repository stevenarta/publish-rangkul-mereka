@extends('layouts.app')

@section('extendedcss')
<link href="{{ asset('css/mobileSupport.css') }}" rel="stylesheet">
@endsection

@section('content')

<div id="pcView">

    {{-- <img src="/img/asset/Banner Dukung Kami.jpg" > --}}
    <div class="image"> <!-- the image container -->
        <img src="storage/img/asset/Banner Dukung Kami.jpg" alt="" style="width: 100%;"/> <!-- the image -->
        <h2 class="imageText" style="text-align: center;">
            <span>"Dukungan itu semudah memperhatikan Sekitar."</span> <!-- span tag to beautify it efficiently -->
        </h2> <!-- the text -->
    </div>


    <div class="myContainer" style="padding-bottom: 4vw;">

        {{-- ====================langkah2 mendukung kampanye=================== --}}
        {{--  68.6  --}}
        <div style="width: 74%; margin: auto; padding-top: 4vw; margin-bottom: 4vw;">
            <div>
                <h2 class="Heading2 text-center mx-auto w-50" style="color: #0E5A89;">
                    Langkah-Langkah untuk Mendukung Kampanye ini.
                </h2>
            </div>
        </div>

        {{-- =====================jenis-jenis kekerasan====================== --}}
        <div class="floating-box shadow" style="display: table; width: 100%; margin-bottom: 4vw;">
            <div style="height: fit-content; display: table-cell; vertical-align: middle; padding: 2vw 0;">
                <div style="text-align: center;">
                    <h1 class="Heading3" style="color: #0E5A89; margin: 2vw 0;">
                        1. Kenali bentuk Kekerasan pada anak yang dapat terjadi
                    </h1>

                    <div style="margin: auto; width: 80%;">
                        @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($kekerasans as $kekerasan)<div style="width: 25%; display: inline-block; vertical-align: top; margin-bottom: 1vw;">
                            <div style="text-align:center;">
                                <a class="AdminBtn EditBtn" href="/kekerasan/edit/{{$i}}">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                            </div>
                            @php
                            $i++;
                            @endphp
                        </div>@endforeach
                        @endif

                        @php
                        $i = 1;
                        @endphp
                        @foreach ($kekerasans as $kekerasan)<div style="width: 25%; display: inline-block; vertical-align: top;">

                            <div style="width: fit-content; margin: auto; margin-bottom: 1vw;height: 12vw;">
                                <img  style="height: 100%; margin:auto;" src="{{asset('rangkulmereka/storage/app/public/'.$kekerasan->img_path)}}">
                            </div>
                            <div style="width: 60%; margin: auto;">
                                <div style="width: fit-content; margin: auto;">
                                    <img style="width: 25%;" src="{{ asset('storage/img/asset/number'.$i.'.png') }}">
                                </div>
                                <p class="Paragraph2" style="line-height: 1.1vw; text-align: justify;">
                                    {{$kekerasan->content}}
                                </p>
                            </div>
                            @php
                            $i++;
                            @endphp
                        </div>@endforeach
                    </div>
                </div>
            </div>
        </div>

        {{-- =========================dampak kekerasan======================= --}}
        <div class="floating-box shadow" style="margin-bottom: 4vw;">
            <div style="padding: 2vw 0 4vw 0; text-align: center;">
                <h3 class="Heading3" style="color: #0E5A89; padding-top: 2vw;">
                    2. Kenali dampak Negatif dari kekerasan yang terjadi
                </h3>
                <p class="Description1" style="text-align: center; width: 29%; margin: auto; margin-bottom: 2vw; line-height: 1.2;">
                    Berikut adalah berbagai dampak negatif akibat kekerasan yang telah terjadi.
                </p>
                @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                <a class="AdminBtn AddBtn" style="vertical-align: middle; text-decoration:none;" href="/dampak/add">
                    <i class="fas fa-plus-square"></i> Add
                </a>
                @endif
                <div style="width: 64.3%; margin: auto; margin-top: 2vw;">
                    <dampak-kekerasan-pagination></dampak-kekerasan-pagination>
                </div>
            </div>
        </div>

        {{-- ======================Siapa pun bisa menjadi Pelaku Kekerasan================== --}}
        <div class="floating-box shadow">
            <div class="text-center" style="padding: 2vw 0;">
                <h3 class="Heading3" style="color: #0E5A89; text-align: center; padding-top: 2vw;">
                    3. Ketahui siapa yang bisa menjadi pelaku kekerasan?
                </h3>

                <div style="width: 49%; margin: 2vw auto;">
                    <img src="storage/img/asset/art/siapapun.png" style="width: 100%;">
                </div>

                <h1 class="Heading3 w-50 mx-auto" style="color: black;margin-bottom: 1vw;text-align: justify;text-align-last: center;">
                    Siapapun dapat melakukannya secara sadar atau tidak sadar, sengaja atau tidak disengaja, termasuk Anda. Apakah anda pernah melakukan kekerasan pada Anak?<br>Ikuti Quiz ini!
                </h1>

                <p class="Description1 w-25 mx-auto" style="text-align: justify; text-align-last: center;line-height: 1.2;margin-top: 1vw;margin-bottom: 1.2vw;">Hasil quiz ini akan sangat membantu kampanye ini dalam bentuk survey, partisipasi, dan edukasi.</p>
                <button type="button" style="margin-bottom: 2vw;" class="btn myBtn shadow mx-auto mt-0 " data-toggle="modal" data-target="#QuizModal">
                    Ikut Quiz
                </button>

            </div>
        </div>

        {{-- =========================Stop Kekerasan======================= --}}
        <div id="SKP" class="floating-box shadow" style="margin: 4vw 0;">
            <div style="padding: 2vw 0 4vw 0; text-align: center;">
                <h3 class="Heading3" style="color: #0E5A89; padding-top: 2vw;">
                    4. Lakukan Aksi untuk Menghentikan Kekerasan pada Anak
                </h3>
                <p class="Description1" style="text-align: center; width: 29%; margin: auto; margin-bottom: 2vw; line-height: 1.2;">
                    Berikut adalah cara untuk menghentikan kekerasan yang terjadi pada anak-anak yang dapat anda lakukan.
                </p>
                @if (Auth::user() != null && Auth::user()->hasRole('Admin'))
                <a class="AdminBtn AddBtn" style="vertical-align: middle; text-decoration:none;" href="/stop-kekerasan/add">
                    <i class="fas fa-plus-square"></i> Add
                </a>
                @endif
                <div style="width: 64.3%; margin: auto; margin-top: 2vw;">
                    <stop-kekerasan-pagination></stop-kekerasan-pagination>
                </div>
            </div>
        </div>

        {{-- ======================Terima kasih atas partisipasi================== --}}
        <div class="floating-box shadow">
            <div class="text-center" style="padding: 2vw 0;">
                <h1 class="Heading3 w-50 mx-auto mb-0" style="color: black; margin-bottom: 1.5vw; text-align: justify; text-align-last: center;">
                    Terima Kasih atas Partisipasi Anda!<br>
                    Ayo bersama kita bangun Indonesia Bebas Kekerasan pada Anak
                </h1>

                <div style="width: 70%; margin: 0 auto 2vw auto;">
                    <img src="{{ asset('storage/img/asset/thank-you.png') }}" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>
</div>






<div id="mobileView">
    {{-- <img src="/img/asset/Banner Dukung Kami.jpg" > --}}
    <div class="image"> <!-- the image container -->
        <img src="storage/img/asset/Banner Dukung Kami.jpg" alt="" style="width: 100%;"/> <!-- the image -->
        <h2 class="imageText" style="text-align: center;">
            <span>"Dukungan itu semudah memperhatikan Sekitar."</span> <!-- span tag to beautify it efficiently -->
        </h2> <!-- the text -->
    </div>


    <div class="myContainer" style="padding-bottom: 4vw;">

        {{-- ====================langkah2 mendukung kampanye=================== --}}
        {{--  68.6  --}}
        <div style="margin: auto; padding: 6vw 0; margin-bottom: 4vw;">
            <div style="margin-bottom: 4vw;">
                <h2 class="Heading2 text-center w-75 mx-auto" style="color: #0E5A89; padding-left: 1.7vw;">
                    Langkah-Langkah untuk Mendukung Kampanye ini.
                </h2>
            </div>
        </div>

        {{-- =====================jenis-jenis kekerasan====================== --}}
        <div class="floating-box shadow" style="margin-bottom: 4vw; padding: 6vw 0;">
            <div class="floating-box-content">
                <div style="text-align: center;">
                    <h1 class="Heading3" style="color: #0E5A89; margin-bottom: 4vw;">
                        1. Kenali bentuk Kekerasan pada anak yang dapat terjadi
                    </h1>

                    <div>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($kekerasans as $kekerasan)<div>

                            <div style="width: fit-content; margin: auto; margin-bottom: 1vw;height: 45vw;">
                                <img  style="height: 100%; margin:auto;" src="{{asset('rangkulmereka/storage/app/public/'.$kekerasan->img_path)}}">
                            </div>
                            <div>
                                <div style="width: fit-content; margin: auto; margin-top: 4vw;">
                                    <img style="width: 25%;" src="{{ asset('storage/img/asset/number'.$i.'.png') }}">
                                </div>
                                <p class="Paragraph2" style="line-height: 1.2; text-align: justify; text-align-last: center;">
                                    {{$kekerasan->content}}
                                </p>
                            </div>
                            @php
                            $i++;
                            @endphp
                        </div>@endforeach
                    </div>
                </div>
            </div>
        </div>

        {{-- =========================dampak kekerasan======================= --}}
        <div class="floating-box shadow" style="margin-bottom: 4vw; padding: 6vw 0;">
            <div class="floating-box-content" style="text-align: center;">
                <h3 class="Heading3" style="color: #0E5A89; padding-bottom: 2vw;">
                    2. Kenali dampak Negatif dari kekerasan yang terjadi
                </h3>
                <p class="Description1 w-75" style="text-align: center; margin: auto; margin-bottom: 2vw; line-height: 1.2;">
                    Berikut adalah berbagai dampak negatif akibat kekerasan yang telah terjadi.
                </p>
                <div style="margin-top: 2vw;">
                    <dampak-kekerasan-pagination></dampak-kekerasan-pagination>
                </div>
            </div>
        </div>

        {{-- ======================Siapa pun bisa menjadi Pelaku Kekerasan================== --}}
        <div class="floating-box shadow" style="margin-bottom: 4vw; padding: 6vw 0;">
            <div class="floating-box-content">
                <div style="maring-bottom: 2vw; text-center">
                    <h3 class="Heading3" style="color: #0E5A89; text-align: center; padding-top: 2vw;">
                        3. Ketahui siapa yang bisa menjadi pelaku kekerasan?
                    </h3>

                    <div style="margin: 4vw auto;">
                        <img src="storage/img/asset/art/siapapun.png" style="width: 100%;">
                    </div>

                    <h1 class="Heading3" style="color: black; text-align: justify; text-align-last: center;">Siapapun dapat melakukannya secara sadar atau tidak sadar, sengaja atau tidak disengaja, termasuk Anda. Apakah anda pernah melakukan kekerasan pada Anak?<br>Ikuti Quiz ini!
                    <p class="Description1" style="text-align: justify; text-align-last: center; line-height: 1.2; margin-top: 1.5vw;">Hasil quiz ini akan sangat membantu kampanye ini dalam bentuk dukungan, partisipasi dan edukasi.</p>
                    <button type="button" class="btn myBtn shadow" data-toggle="modal" data-target="#QuizModal">
                        Ikut Quiz
                    </button>
                </div>
            </div>
        </div>

        {{-- =========================Stop Kekerasan======================= --}}
        <div id="SKM" class="floating-box shadow" style="margin-bottom: 4vw; padding: 6vw 0;">
            <div class="floating-box-content" style="text-align: center;">
                <h3 class="Heading3" style="text-align: justify; text-align-last: center; color: #0E5A89; padding-bottom: 2vw;">
                    4. Lakukan Aksi untuk Menghentikan Kekerasan pada Anak
                </h3>
                <p class="Description1" style="text-align: justify; text-align-last: center;;margin-bottom: 2vw; line-height: 1.2;">
                    Berikut adalah cara untuk menghentikan kekerasan yang terjadi pada anak-anak yang dapat anda lakukan.
                </p>

                <div style="margin-top: 2vw;">
                    <stop-kekerasan-pagination></stop-kekerasan-pagination>
                </div>
            </div>
        </div>

        {{-- ======================Terima kasih atas partisipasi================== --}}
        <div class="floating-box shadow">
            <div class="text-center" style="padding: 2vw 0;">
                <h1 class="Heading3 mb-0" style="width: 85%; color: black; margin: 1.5vw auto 6vw auto; text-align: justify; text-align-last: center;">
                    Terima Kasih atas Partisipasi Anda!<br>
                    Ayo bersama kita bangun Indonesia Bebas Kekerasan pada Anak
                </h1>

                <div style="width: 100%; margin: 0 auto 2vw auto;">
                    <img src="{{ asset('storage/img/asset/thank-you.png') }}" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
