@extends('layouts.app')

@section('content')
<div class="myContainer" style="padding: 4vw 0;">
    <div class="floating-box shadow">
        <div class="myContainer" style="padding: 4vw 0;">
            <form method="post" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label for="carouselId">Carousel Id</label>
                    <select class="form-control" name="carouselId" id="carouselId">
                        @foreach ($carousels as $carousel)
                        <option>{{$carousel->id}}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
