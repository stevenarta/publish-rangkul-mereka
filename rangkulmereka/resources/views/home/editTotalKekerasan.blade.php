@extends('layouts.app')

@section('content')
<div class="myContainer" style="padding: 4vw 0;">
    <div class="floating-box shadow">
        <div class="myContainer" style="padding: 4vw 0;">
            <form method="post">
                @csrf
                <div class="form-group">
                    <label for="total">Total Kekerasan</label>
                    <input type="number" class="form-control @error('total') is-invalid @enderror" name="total" id="total" value="{{$totalKekerasan->total}}" placeholder="total">

                    @error('total')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
