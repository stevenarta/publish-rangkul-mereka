@extends('layouts.app')

@section('content')
<div class="myContainer" style="padding: 4vw 0;">
    <div class="floating-box shadow">
        <div class="myContainer" style="padding: 4vw 0;">
            <form method="post" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label for="carouselId">Carousel Id</label>
                    <select class="form-control" name="carouselId" id="carouselId">
                        @foreach ($carousels as $carousel)
                        <option>{{$carousel->id}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" class="form-control-file @error('image') is-invalid @enderror" name="image" id="image">

                    @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="BlogId">Blog</label>
                    <select class="form-control" name="BlogId" id="BlogId">
                        @foreach ($blogs as $blog)
                        <option value="{{$blog->id}}">{{$blog->id}} - {{$blog->title}}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
