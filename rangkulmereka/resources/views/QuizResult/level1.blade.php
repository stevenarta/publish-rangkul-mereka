@extends('layouts.app')

@section('metadata')
    <meta property="og:image:secure_url" itemprop="image" content="{{asset('storage/img/asset/art/Hasilquiz-03.png')}}" />
    <meta property="og:image" itemprop="image" content="{{asset('storage/img/asset/art/Hasilquiz-03.png')}}" />
    <meta property="og:title" content="Ternyata Aku berada pada tingkat menengah" />
    <meta property="og:description" content="Jarang melakukan tindakan kekerasan memang tidak buruk. Tetapi sebisa mungkin tetap hindarilah bentuk kekerasan fisik, dan penggunaan kata-kata yang kasar terhadap anak-anak. Anda bisa merubah kebiasaan buruk ini dengan cara bersikap lebih bijak & tenang terhadap anak-anak." />
    <meta property="og:type"  content="website" />
    <meta property="og:image:width" content="256">
    <meta property="og:image:height" content="256">
@endsection

@section('content')
<div id="pcView">
    <div class="container"style="width:82%; max-width: none; padding-bottom: 4vw;">

        <div style="margin: 1vw auto 1vw auto;">

            <a href="/" role="button" style="font-size: 1vw; margin-right: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Beranda</a>

            <span style="font-size: 1.5vw; vertical-align: middle;"> > </span>

            <a href="/support" role="button" style="font-size: 1vw; margin-right: 0.2vw; margin-left: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Dukung Kami</a>

            <span style="font-size: 1.5vw; vertical-align: middle;"> > </span>

            <a href="#" role="button" style="font-size: 1vw; margin-left: 0.2vw; text-decoration: none; color: #0f75bd; font-family: 'futuraMedium'; font-weight: bold; vertical-align: middle;">Ikut Quiz</a>

        </div>

        <h2 class="Heading2" style="text-align:center;">Terima Kasih untuk Partisipasinya !</h2>

        <p class="Description1" style="text-align-last: center;width: 30.3%;text-align: justify;margin: auto;line-height: 1.4;margin-bottom: 2vw;"> Hasil quiz ini akan sangat membantu kampanye ini dalam bentuk survey, partisipasi dan edukasi.</p>

        <div class="floating-box shadow" style="width: 77.7%; margin: auto;">
            <div style="height: 15vw; margin: auto; width: fit-content; margin-top: 1vw; margin-bottom: 2vw;">
                <img src="{{asset('storage/img/asset/art/Hasilquiz-03.png')}}" alt="" style="height: 100%;">
            </div>

            <h3 class="Heading3" style="text-align: center; color: #0E5A89;">
                Ternyata Anda berada pada tingkat menengah<br>
                “Jarang melakukan tindak kekerasan”
            </h3>

            <p class="Paragraph1" style="text-align: justify; text-align-last: center;width: 48.8%; margin: auto; line-height: 1.2; margin-top: 1vw; margin-bottom: 2vw;">
                Jarang melakukan tindakan kekerasan memang tidak buruk.<br> Tetapi sebisa mungkin tetap hindarilah bentuk kekerasan fisik, dan penggunaan kata-kata yang kasar terhadap anak-anak. Anda bisa merubah kebiasaan buruk ini dengan cara bersikap lebih bijak & tenang terhadap anak-anak.
            </p>

            <h3 class="Heading3" style="text-align: center; color: #0E5A89;">
                Dampak Negatif pada Tingkat Menengah
            </h3>

            {{-- =====================Dampak Negatif====================== --}}
            <div style="margin: auto; width: 74%;">

                <div style="width: 33.3%; display: inline-block; vertical-align: top;">

                    <div style="width: fit-content; margin: auto; margin-bottom: 1vw;height: 12vw;">

                        <img  style="height: 100%; margin:auto;" src="{{asset('storage/img/asset/art/denda.png')}}">

                    </div>

                    <div style="width: 70.5%; margin: auto;">

                        <div style="width: 25%; margin: auto;">

                            <img style="width: 100%;" src="{{ asset('storage/img/asset/number1.png') }}">

                        </div>

                        <p class="Paragraph2" style="line-height: 1.1vw; text-align: justify;">

                            Pelaku dapat terjerat hukum dengan pasal dan undang- undang yang berhubungan dengan tindakan  kekerasan bahkan dapat berujung pada pidana.

                        </p>

                    </div>

                </div><div style="width: 33.3%; display: inline-block; vertical-align: top;">

                    <div style="width: fit-content; margin: auto; margin-bottom: 1vw;height: 12vw;">

                        <img  style="height: 100%; margin:auto;" src="{{asset('storage/img/asset/art/depresi.png')}}">

                    </div>

                    <div style="width: 70.5%; margin: auto;">

                        <div style="width: 25%; margin: auto;">

                            <img style="width: 100%;" src="{{ asset('storage/img/asset/number2.png') }}">

                        </div>

                        <p class="Paragraph2" style="line-height: 1.1vw; text-align: justify;">

                            Korban anak dapat mengalami dampak negatif di masa yang akan datang seperti prilaku merokok, minum-minuman keras, tidak percaya diri, emosional, dan lain-lain.

                        </p>

                    </div>

                </div><div style="width: 33.3%; display: inline-block; vertical-align: top;">

                    <div style="width: fit-content; margin: auto; margin-bottom: 1vw;height: 12vw;">

                        <img  style="height: 100%; margin:auto;" src="{{asset('storage/img/asset/art/kecewa.png')}}">

                    </div>

                    <div style="width: 70.5%; margin: auto;">

                        <div style="width: 25%; margin: auto;">

                            <img style="width: 100%;" src="{{ asset('storage/img/asset/number3.png') }}">

                        </div>

                        <p class="Paragraph2" style="line-height: 1.1vw; text-align: justify;">

                            Hubungan Anda dan korban anak menjadi kurang akrab. dan anak menjadi kehilangan rasa percaya kepada Anda.

                        </p>

                    </div>

                </div>

            </div>

            <h3 class="Heading3 mx-auto mt-5" style="text-align: center; color: #0E5A89;width: 48.8%; ">
                Lanjutkan untuk tahu solusi dan tips untuk menghentikan kekerasan terhadap anak.
            </h3>

            <div class="text-center mb-5">
                <a href="/support/#SKP" class="btn myBtn shadow">
                    Lanjutkan
                </a>
            </div>

            <div style="text-align:center; margin-bottom: 2vw; margin-top: 1vw;">

                <p class="Heading3 m-0" style="display: inline-block;vertical-align: middle; color: #0E5A89;">Bagikan</p>



                <div style="display: inline-block; height: 3.5vw; width: 3.5vw; position: relative;vertical-align: middle; margin-left:1.5vw;">

                    <div style="display: table; margin: auto;">

                        <div data-network="facebook" class="st-custom-button facebookButton shadow" style="display: table-cell; vertical-align: middle;">

                            <i class="fab fa-facebook-f socialLogo" style="display: block; margin: auto;"></i>

                        </div>

                    </div>

                </div>
                <div style="display: inline-block; height: 3.5vw; width: 3.5vw;vertical-align: middle;">

                    <div style="display: table; margin: auto;">

                        <div data-network="whatsapp" class="st-custom-button whatsAppButton shadow" data-url="https://rangkulmereka.asibu.com/result/lvl1" style="display: table-cell; vertical-align: middle;">

                            <i class="fab fa-whatsapp socialLogo" style="display: block; margin: auto;"></i>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div id="mobileView">
    <div class="container"style="width:82%; max-width: none; padding-bottom: 4vw;">
        <div style="margin: 4vw auto;">
            <a href="/" role="button" style="font-size: 2.4vw; margin-right: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Beranda</a>
            <span style="font-size: 2.4vw; vertical-align: middle;"> > </span>
            <a href="/blog" role="button" style="font-size: 2.4vw; margin-right: 0.2vw; margin-left: 0.2vw; text-decoration: none; color: black; font-family: 'futuraMedium'; vertical-align: middle;">Dukung Kami</a>
            <span style="font-size: 2.4vw; vertical-align: middle;"> > </span>
            <a href="#" role="button" style="font-size: 2.4vw; margin-left: 0.2vw; text-decoration: none; color: #0f75bd; font-family: 'futuraMedium'; font-weight: bold; vertical-align: middle;">Ikut Quiz</a>
        </div>
    </div>
    <div class="floating-box-content">
        <h2 class="Heading2" style="text-align:center; margin-bottom: 2vw;">Terima Kasih untuk Partisipasinya !</h2>

        <p class="Description1" style="text-align-last: center;text-align: justify;margin: auto;line-height: 1.2;margin-bottom: 2vw;"> Hasil quiz ini akan sangat membantu kampanye ini dalam bentuk survey, partisipasi dan edukasi.</p>
    </div>
    <div class="myContainer">
        <div class="floating-box shadow" style="padding: 6vw 0;">
            <div class="floating-box-content">

                <div style="height: 40vw; margin: auto; width: fit-content; margin-top: 1vw; margin-bottom: 2vw;">

                    <img src="{{asset('storage/img/asset/art/Hasilquiz-03.png')}}" alt="" style="height: 100%;">

                </div>

                <h3 class="Heading3" style="text-align: center; color: #0E5A89;">
                    Ternyata Anda berada pada tingkat menengah<br>
                    “Jarang melakukan tindak kekerasan”
                </h3>

                <p class="Paragraph1" style="text-align: justify; text-align-last: center; line-height: 1.2; margin-top: 4vw; margin-bottom: 8vw;">

                    Jarang melakukan tindakan kekerasan memang tidak buruk.<br>Tetapi sebisa mungkin tetap hindarilah bentuk kekerasan fisik, dan penggunaan kata-kata yang kasar terhadap anak-anak. Anda bisa merubah kebiasaan buruk ini dengan cara bersikap lebih bijak & tenang terhadap anak-anak.

                </p>

                <div style="text-align: center;">
                    <h1 class="Heading3" style="color: #0E5A89; margin-bottom: 4vw;">
                        Dampak Negatif pada Tingkat Menengah
                    </h1>

                    <div>
                        <div style="width: fit-content; margin: auto; margin-bottom: 1vw;height: 45vw;">
                            <img  style="height: 100%; margin:auto;" src="{{asset('storage/img/asset/art/denda.png')}}">
                        </div>
                        <div>
                            <div style="width: fit-content; margin: auto; margin-top: 4vw;">
                                <img style="width: 25%;" src="{{ asset('storage/img/asset/number1.png') }}">
                                </div>
                                <p class="Paragraph2" style="line-height: 1.2; text-align: justify;">
                                    Pelaku dapat terjerat hukum dengan pasal dan undang- undang yang berhubungan dengan tindakan  kekerasan bahkan dapat berujung pada pidana.
                                </p>
                        </div>
                    </div>

                    <div>
                        <div style="width: fit-content; margin: auto; margin-bottom: 1vw;height: 45vw;">
                            <img  style="height: 100%; margin:auto;" src="{{asset('storage/img/asset/art/depresi.png')}}">
                        </div>
                        <div>
                            <div style="width: fit-content; margin: auto; margin-top: 4vw;">
                                <img style="width: 25%;" src="{{ asset('storage/img/asset/number2.png') }}">
                                </div>
                                <p class="Paragraph2" style="line-height: 1.2; text-align: justify;">
                                    Korban anak dapat mengalami dampak negatif di masa yang akan datang seperti prilaku merokok, minum-minuman keras, tidak percaya diri, emosional, dan lain-lain.
                                </p>
                        </div>
                    </div>

                    <div>
                        <div style="width: fit-content; margin: auto; margin-bottom: 1vw;height: 45vw;">
                            <img  style="height: 100%; margin:auto;" src="{{asset('storage/img/asset/art/kecewa.png')}}">
                        </div>
                        <div>
                            <div style="width: fit-content; margin: auto; margin-top: 4vw;">
                                <img style="width: 25%;" src="{{ asset('storage/img/asset/number3.png') }}">
                                </div>
                                <p class="Paragraph2" style="line-height: 1.2; text-align: justify; margin-bottom: 8vw;">
                                    Hubungan Anda dan korban anak menjadi kurang akrab. dan anak menjadi kehilangan rasa percaya kepada Anda.
                                </p>
                        </div>
                    </div>
                </div>

                <h3 class="Heading3 mx-auto mb-3" style="text-align: center; color: #0E5A89;">
                    Lanjutkan untuk tahu solusi dan tips untuk menghentikan kekerasan terhadap anak.
                </h3>

                <div class="text-center mb-4">
                    <a href="/support/#SKM" class="btn myBtn shadow">
                        Lanjutkan
                    </a>
                </div>

                <div class="pt-2" style="text-align:center;">

                    <p class="Heading3 m-0" style="display: inline-block;vertical-align: middle; color: #0E5A89;">Bagikan</p>



                    <div style="display: inline-block; position: relative;vertical-align: middle; margin-left:1.5vw;">

                        <div style="display: table; margin: auto;">

                            <div data-network="facebook" class="st-custom-button facebookButton shadow" style="display: table-cell; vertical-align: middle;">

                                <i class="fab fa-facebook-f socialLogo" style="display: block; margin: auto;"></i>

                            </div>

                        </div>

                    </div>
                    <div style="display: inline-block; margin-left:1.5vw; vertical-align: middle;">

                        <div style="display: table; margin: auto;">

                            <div data-network="whatsapp" class="st-custom-button whatsAppButton shadow" data-url="https://rangkulmereka.asibu.com/result/lvl1" style="display: table-cell; vertical-align: middle;">

                                <i class="fab fa-whatsapp socialLogo" style="display: block; margin: auto;"></i>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="height: 4vw;"></div>
</div>

@endsection

