<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StopKekerasan extends Model
{
    protected $fillable = [
        'img_path', 'title', 'desc'
    ];
}
