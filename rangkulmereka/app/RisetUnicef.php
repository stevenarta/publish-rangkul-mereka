<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RisetUnicef extends Model
{
    protected $fillable = [
        'img_path', 'content'
    ];
}
