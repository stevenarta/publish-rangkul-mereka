<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dampak extends Model
{
    protected $fillable = [
        'img_path','title','desc'
    ];
}
