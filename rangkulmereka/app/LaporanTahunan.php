<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaporanTahunan extends Model
{
    protected $fillable = [
        'img_path', 'content', 'list'
    ];
}
