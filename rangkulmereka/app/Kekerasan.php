<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kekerasan extends Model
{
    protected $fillable = [
        'img_path', 'content'
    ];
}
