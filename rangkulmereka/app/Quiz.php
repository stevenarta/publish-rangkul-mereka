<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $fillable = [
        'question', 'option1', 'option2', 'option3', 'point1', 'point2', 'point3'
    ];
}
