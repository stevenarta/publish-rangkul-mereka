<?php

namespace App\Http\Controllers;

use App\Quiz;
use Illuminate\Http\Request;

class AdminQuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            "quizzes" => Quiz::all()
        ];

        return view('Quiz.adminIndex')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Quiz.adminCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'question' => ['required'],
            'option1' => ['required'],
            'option2' => ['required'],
            'option3' => ['required'],
            'point1' => ['required'],
            'point2' => ['required'],
            'point3' => ['required'],
        ]);

        Quiz::create([
            'question' => $request->question,
            'option1' => $request->option1,
            'option2' => $request->option2,
            'option3' => $request->option3,
            'point1' => $request->point1,
            'point2' => $request->point2,
            'point3' => $request->point3
        ]);

        return redirect('/admin/quiz');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            "quiz" => Quiz::where('id', $id)->first()
        ];

        return view('Quiz.adminEdit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'question' => ['required'],
            'option1' => ['required'],
            'option2' => ['required'],
            'option3' => ['required'],
            'point1' => ['required'],
            'point2' => ['required'],
            'point3' => ['required'],
        ]);

        Quiz::where('id', $id)
            ->update([
                'question' => $request->question,
                'option1' => $request->option1,
                'option2' => $request->option2,
                'option3' => $request->option3,
                'point1' => $request->point1,
                'point2' => $request->point2,
                'point3' => $request->point3
            ]);

        return redirect('/admin/quiz')->with('status', ' Item Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Quiz::where('id', $id)->first()->delete();
        return redirect('/admin/quiz')->with('status', ' Item Successfully Removed');
    }
}
