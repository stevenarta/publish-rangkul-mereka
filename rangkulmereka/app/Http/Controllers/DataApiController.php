<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Carousel;
use App\Dampak;
use App\Quiz;
use App\StopKekerasan;
use App\UserAnswer;
use App\UserScore;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DataApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBlog($category = null)
    {

        if($category != null){
            $blogs = Blog::where('category', $category)->orderBy('created_at', 'DESC')->paginate(6);
        }else{
            $blogs = Blog::orderBy('created_at', 'DESC')->paginate(6);
        }

        $isAdmin = false;

        if(Auth::user() != null){
            if(Auth::user()->hasRole('Admin')){
                $isAdmin = true;
            }
        }

        $data = [
            "isAdmin" => $isAdmin,
            "blogs" => $blogs
        ];

        return response()->json($data);
    }

    public function getDampak()
    {
        $isAdmin = false;

        if(Auth::user() != null){
            if(Auth::user()->hasRole("Admin")){
                $isAdmin = true;
            }
        }

        $dampaks = Dampak::paginate(1);
        $data = [
            "isAdmin" => $isAdmin,
            "dampaks" => $dampaks
        ];

        return response()->json($data);
    }

    public function getStopKekerasan()
    {
        $isAdmin = false;

        if(Auth::user() != null){
            if(Auth::user()->hasRole('Admin')){
                $isAdmin = true;
            }
        }

        $items = StopKekerasan::paginate(1);
        $data = [
            "isAdmin" => $isAdmin,
            "items" => $items
        ];

        return response()->json($data);
    }

    public function getQuiz()
    {
        $isAdmin = false;

        if(Auth::user() != null){
            if(Auth::user()->hasRole('Admin')){
                $isAdmin = true;
            }
        }

        $quizzes = Quiz::paginate(1);
        $data = [
            "isAdmin" => $isAdmin,
            "quizzes" => $quizzes,
        ];

        return response()->json($data);
    }

    public function getCarousel()
    {
        $isAdmin = false;

        if(Auth::user() != null){
            if(Auth::user()->hasRole('Admin')){
                $isAdmin = true;
            }
        }

        $carousels = Carousel::paginate(1);
        $data = [
            "isAdmin" => $isAdmin,
            "carousels" => $carousels,
        ];

        return response()->json($data);
    }

    public function insertUserScore($score)
    {
        UserScore::create([
            'user_id' => Auth::user()->id,
            'score' => $score
        ]);

        $data = [
            "result" => $score
        ];

        return response()->json($data);
    }
}
