<?php

namespace App\Http\Controllers;

use App\Instagram;
use Illuminate\Http\Request;

class AboutController extends Controller
{

    public function index()
    {
        $data = [
            "ig" => Instagram::all(),
            "igCount" => Instagram::count()
        ];

        return view('About.index')->with($data);
    }

    public function IgCreate()
    {
        return view('About.addIg');
    }

    public function IgStore(Request $request)
    {
        $request->validate([
            'url' => ['required'],
        ]);

        Instagram::create([
            'url' => $request->url,
        ]);

        if(Instagram::count() > 4){
            Instagram::all()->first()->delete();
        }

        return redirect('/about');
    }

    public function IgEdit($id)
    {
        $ig = Instagram::where('id', $id)->first();

        return view('About.editIg', compact('ig'));
    }

    public function IgUpdate(Request $request, $id)
    {
        $request->validate([
            'url' => ['required'],
        ]);

        Instagram::where('id', $id)
            ->update([
                'url' => $request->url
            ]);

        return redirect('/about');
    }

    public function IgDestroy($id)
    {
        // dump($id);
        Instagram::where('id', $id)->delete();
        return redirect('/about');
    }
}
