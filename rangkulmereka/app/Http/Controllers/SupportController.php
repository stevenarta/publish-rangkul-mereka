<?php

namespace App\Http\Controllers;

use App\Dampak;
use App\Kekerasan;
use App\StopKekerasan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SupportController extends Controller
{
    public function index()
    {
        $data = [
            'kekerasans' => Kekerasan::all()
        ];

        return view('Support.index')->with($data);
    }

    public function kekerasanEdit($id)
    {
        $kekerasan = Kekerasan::where('id', $id)->first();

        return view('Support.editKekerasan', compact('kekerasan'));
    }

    public function kekerasanUpdate(Request $request, $id)
    {
        $request->validate([
            'image' => ['image','max:100000','mimes:jpg,jpeg,png'],
            'desc' => ['required'],
        ]);

        if($request->image != null){
            $temp = Kekerasan::where('id', $id)->first();
            Storage::delete($temp->img_path);

            $request->file('image')->storeAs(
                'img/kekerasan_img',
                'kekerasan_'.$id.'.'.$request->image->getClientOriginalExtension()
            );

            Kekerasan::where('id', $id)->update([
                'img_path' => 'img/kekerasan_img/kekerasan_'.$id.'.'.$request->image->getClientOriginalExtension(),
                'content' => $request->desc
            ]);
        }else{
            Kekerasan::where('id', $id)->update([
                'content' => $request->desc
            ]);
        }
        return redirect('/support');
    }


    // =====================================dampak=====================================
    public function dampakCreate()
    {
        return view('Support.addDampak');
    }

    public function dampakStore(Request $request)
    {
        $request->validate([
            'image' => ['required','image','max:100000','mimes:jpg,jpeg,png'],
            'title' => ['required'],
            'desc' => ['required'],
        ]);

        $dampak = Dampak::create([
            'img_path' => " ",
            'title' => $request->title,
            'desc' => $request->desc
        ]);

        $request->file('image')->storeAs(
            'img/dampak_img',
            'dampak_'.$dampak->id.'.'.$request->image->getClientOriginalExtension()
        );

        Dampak::where('id', $dampak->id)->update([
            'img_path' => 'img/dampak_img/dampak_'.$dampak->id.'.'.$request->image->getClientOriginalExtension()
        ]);

        return redirect('/support');
    }

    public function dampakEdit($id)
    {
        $dampak = Dampak::where('id', $id)->first();

        return view('Support.editDampak', compact('dampak'));
    }

    public function dampakUpdate(Request $request, $id)
    {
        $request->validate([
            'image' => ['image','max:100000','mimes:jpg,jpeg,png'],
            'title' => ['required'],
            'desc' => ['required'],
        ]);

        if($request->image != null){
            $temp = Dampak::where('id', $id)->first();
            Storage::delete($temp->img_path);

            $request->file('image')->storeAs(
                'img/dampak_img',
                'dampak_'.$id.'.'.$request->image->getClientOriginalExtension()
            );

            Dampak::where('id', $id)->update([
                'img_path' => 'img/dampak_img/dampak_'.$id.'.'.$request->image->getClientOriginalExtension(),
                'title' => $request->title,
                'desc' => $request->desc
            ]);
        }else{
            Dampak::where('id', $id)->update([
                'title' => $request->title,
                'desc' => $request->desc
            ]);
        }

        return redirect('/support');
    }

    public function dampakDestroy($id)
    {
        $dampak = Dampak::where('id', $id)->first();
        Storage::delete($dampak->img_path);

        $dampak->delete();

        return redirect('/support');
    }

    // =====================================stop kekerasan=====================================
    public function stopCreate()
    {
        return view('Support.addStop');
    }

    public function stopStore(Request $request)
    {
        $request->validate([
            'image' => ['required','image','max:100000','mimes:jpg,jpeg,png'],
            'title' => ['required'],
            'desc' => ['required'],
        ]);

        $item = StopKekerasan::create([
            'img_path' => " ",
            'title' => $request->title,
            'desc' => $request->desc
        ]);

        $request->file('image')->storeAs(
            'img/stop_kekerasan_img',
            'item_'.$item->id.'.'.$request->image->getClientOriginalExtension()
        );

        StopKekerasan::where('id', $item->id)->update([
            'img_path' => 'img/stop_kekerasan_img/item_'.$item->id.'.'.$request->image->getClientOriginalExtension()
        ]);

        return redirect('/support');
    }

    public function stopEdit($id)
    {
        $item = StopKekerasan::where('id', $id)->first();

        return view('Support.editStop', compact('item'));
    }

    public function stopUpdate(Request $request, $id)
    {
        $request->validate([
            'image' => ['image','max:100000','mimes:jpg,jpeg,png'],
            'title' => ['required'],
            'desc' => ['required'],
        ]);

        if($request->image != null){

            $temp = StopKekerasan::where('id', $id)->first();
            Storage::delete($temp->img_path);

            $request->file('image')->storeAs(
                'img/stop_kekerasan_img',
                'item_'.$id.'.'.$request->image->getClientOriginalExtension()
            );

            StopKekerasan::where('id', $id)->update([
                'img_path' => 'img/stop_kekerasan_img/item_'.$id.'.'.$request->image->getClientOriginalExtension(),
                'title' => $request->title,
                'desc' => $request->desc
            ]);
        }else{
            StopKekerasan::where('id', $id)->update([
                'title' => $request->title,
                'desc' => $request->desc
            ]);
        }

        return redirect('/support');
    }

    public function stopDestroy($id)
    {
        $item = StopKekerasan::where('id', $id)->first();
        Storage::delete($item->img_path);

        $item->delete();

        return redirect('/support');
    }
}
