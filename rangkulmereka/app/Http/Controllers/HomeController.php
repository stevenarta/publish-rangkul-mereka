<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Carousel;
use App\Instagram;
use App\TotalKekerasan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $temp = TotalKekerasan::where('id', 1)->first()->total;

        $items = strval($temp);
        $total = "";
        if(strlen($items) < 6){
            for ($i=0; $i < (6 - strlen($items)); $i++) {
                $total .= "0";
            }
            $total .= $items;
        }else{
            $total = $items;
        }

        $carousels = Carousel::all();

        $data = [
            "totalKekerasan" => $total,
            "blogs" => Blog::simplepaginate(6),
            "mostViewedBlog" => Blog::orderBy('views', 'DESC')->take(3)->get(),
            "carousels" => $carousels
        ];

        return view('home.index')->with($data);
    }

    public function totalKekerasanEdit()
    {
        $totalKekerasan = TotalKekerasan::where('id', 1)->first();

        return view('home.editTotalKekerasan', compact('totalKekerasan'));
    }

    public function totalKekerasanUpdate(Request $request)
    {
        $request->validate([
            'total' => ['required','max:6'],
        ]);

        TotalKekerasan::where('id', 1)
            ->update([
                'total' => $request->total
            ]);

        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function carouselCreate()
    {

        $data = [
            "blogs" => Blog::all()
        ];

        return view('home.addCarousel')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function carouselStore(Request $request)
    {
        $request->validate([
            'image' => ['required','image','max:100000','mimes:jpg,jpeg,png'],
            'BlogId' => ['required']
        ]);

        $carousel = Carousel::create([
            'blog_id' => $request->BlogId,
            'img_path' => ''
        ]);

        $request->file('image')->storeAs(
            'img/carousel_img',
            'carousel_'.$carousel->id.'.'.$request->image->getClientOriginalExtension()
        );

        Carousel::where('id', $carousel->id)->update([
            'img_path' => 'img/carousel_img/carousel_'.$carousel->id.'.'.$request->image->getClientOriginalExtension(),
            'blog_id' => $request->BlogId,
        ]);

        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function carouselEdit()
    {

        $data = [
            "carousels" => Carousel::all(),
            "blogs" => Blog::all()
        ];

        return view('home.editCarousel')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function carouselUpdate(Request $request)
    {
        $request->validate([
            'carouselId' => ['required'],
            'image' => ['image','max:100000','mimes:jpg,jpeg,png'],
            'BlogId' => ['required'],
        ]);

        if($request->image != null){
            $temp = Carousel::where('id', $request->carouselId)->first();
            Storage::delete($temp->img_path);

            $request->file('image')->storeAs(
                'img/carousel_img',
                'carousel_'.$request->carouselId.'.'.$request->image->getClientOriginalExtension()
            );

            Carousel::where('id', $request->carouselId)->update([
                'img_path' => 'img/carousel_img/carousel_'.$request->carouselId.'.'.$request->image->getClientOriginalExtension(),
                'blog_id' => $request->BlogId
            ]);
        }else{
            Carousel::where('id', $request->carouselId)->update([
                'blog_id' => $request->BlogId
            ]);
        }

        return redirect('/')->with('status', ' Item Successfully Updated');
    }

    public function carouselDelete()
    {

        $data = [
            "carousels" => Carousel::all(),
        ];

        return view('home.deleteCarousel')->with($data);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function carouselDestroy(Request $request)
    {
        $temp = Carousel::where('id', $request->carouselId)->first();
        Storage::delete($temp->img_path);

        Carousel::where('id', $request->carouselId)->delete();
        return redirect('/')->with('status', ' Item Successfully Removed');
    }
}
