<?php

namespace App\Http\Controllers;

use App\Blog;
use Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use SebastianBergmann\Environment\Console;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            "mostViewedBlog" => Blog::orderBy('views', 'DESC')->take(3)->get(),
        ];

        return view('Blog.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'max:100'],
            'subtitle' => ['required', 'max:100'],
            'category' => ['required'],
            'desc' => ['required'],
            'url' => [''],
            'image' => ['required','image','max:100000','mimes:jpg,jpeg,png'],
            'postedBy' => ['required'],
            ]
        );

        $blog = Blog::create([
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'category' => $request->category,
            'desc' => $request->desc,
            'url' => $request->url,
            'postedBy' => $request->postedBy
        ]);

        $request->file('image')->storeAs(
            'img/blog_img',
            'blog_'.$blog->id.'.'.$request->image->getClientOriginalExtension()
        );
        // $request->image->move('img/blog_img','blog_'.$blog->id.'.'.$request->image->getClientOriginalExtension());

        Blog::where('id', $blog->id)->update([
            'image' => 'img/blog_img/blog_'.$blog->id.'.'.$request->image->getClientOriginalExtension()
        ]);

        return redirect('/blog');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            "blog" => Blog::where('id', $id)->first(),
            "anotherBlog" => Blog::all()->take(6)
        ];

        return view('Blog.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::where('id', $id)->first();

        return view('Blog.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => ['required', 'max:100'],
            'subtitle' => ['required', 'max:100'],
            'category' => ['required'],
            'desc' => ['required'],
            'url' => [],
            'image' => ['image','max:100000','mimes:jpg,jpeg,png'],
            'postedBy' => ['required'],
        ]);

        if($request->image != null){
            $temp = Blog::where('id', $id)->first();
            Storage::delete($temp->image);

            Blog::where('id', $id)
                ->update([
                    'title' => $request->title,
                    'subtitle' => $request->subtitle,
                    'category' => $request->category,
                    'desc' => $request->desc,
                    'url' => $request->url,
                    'postedBy' => $request->postedBy
                ]);

            $request->file('image')->storeAs(
                'img/blog_img',
                'blog_'.$id.'.'.$request->image->getClientOriginalExtension()
            );
            // $request->image->move('img/blog_img','blog_'.$id.'.'.$request->image->getClientOriginalExtension());

            Blog::where('id', $id)->update([
                'image' => 'img/blog_img/blog_'.$id.'.'.$request->image->getClientOriginalExtension()
            ]);
        }else{
            Blog::where('id', $id)
                ->update([
                    'title' => $request->title,
                    'subtitle' => $request->subtitle,
                    'category' => $request->category,
                    'desc' => $request->desc,
                    'url' => $request->url,
                    'postedBy' => $request->postedBy
                ]);
        }

        return redirect('/blog')->with('status', ' Item Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $temp = Blog::where('id', $id)->first();
        Storage::delete($temp->image);

        Blog::where('id', $id)->delete();
        return redirect('/blog')->with('status', ' Item Successfully Removed');
    }
}
