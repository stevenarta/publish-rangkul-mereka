<?php

namespace App\Http\Controllers;

use App\UserEmail;
use Illuminate\Http\Request;

class UserEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserEmail  $userEmail
     * @return \Illuminate\Http\Response
     */
    public function show(UserEmail $userEmail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserEmail  $userEmail
     * @return \Illuminate\Http\Response
     */
    public function edit(UserEmail $userEmail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserEmail  $userEmail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserEmail $userEmail)
    {
        $request->validate([
            'email' => ['email']
        ]);

        $temp = UserEmail::where('email', $request->email)->first();

        if($temp == null){
            UserEmail::create([
                'email' => $request->email
            ]);
        };

        return redirect('/about');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserEmail  $userEmail
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserEmail $userEmail)
    {
        //
    }
}
