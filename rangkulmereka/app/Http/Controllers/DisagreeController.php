<?php

namespace App\Http\Controllers;

use App\LaporanTahunan;
use App\RisetUnicef;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class DisagreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($page)
    {
        $data = [
            'laporan' => LaporanTahunan::where('id', 1)->first(),
            'risets' => RisetUnicef::all()
        ];

        if($page == 1){
            return view('Disagree.index')->with($data);
        }else{
            return view('Disagree.sayangSekali')->with($data);
        }

    }

    public function laporanEdit()
    {
        $laporan = LaporanTahunan::where('id', 1)->first();

        return view('Disagree.editLaporan', compact('laporan'));
    }

    public function laporanUpdate(Request $request)
    {
        $request->validate([
            'image' => ['image','max:100000','mimes:jpg,jpeg,png'],
            'content' => ['required'],
            'list' => ['required'],
        ]);

        if($request->image != null){
            $temp = LaporanTahunan::where('id', 1)->first();
            Storage::delete($temp->img_path);

            $request->file('image')->storeAs(
                'img/laporan_img',
                'laporan.'.$request->image->getClientOriginalExtension()
            );

            LaporanTahunan::where('id', 1)
            ->update([
                'img_path' => 'img/laporan_img/laporan.'.$request->image->getClientOriginalExtension(),
                'content' => $request->content,
                'list' => $request->list
            ]);
        }else{
            LaporanTahunan::where('id', 1)
            ->update([
                'content' => $request->content,
                'list' => $request->list
            ]);
        }

        return redirect('/disagree');
    }

    public function risetEdit($id)
    {
        $riset = RisetUnicef::where('id', $id)->first();

        return view('Disagree.editRiset', compact('riset'));
    }

    public function risetUpdate(Request $request, $id)
    {
        $request->validate([
            'image' => ['image','max:100000','mimes:jpg,jpeg,png'],
            'desc' => ['required'],
        ]);

        if($request->image != null){
            $temp = RisetUnicef::where('id', $id)->first();
            Storage::delete($temp->img_path);

            $request->file('image')->storeAs(
                'img/unicef_img',
                'riset_'.$id.'.'.$request->image->getClientOriginalExtension()
            );

            RisetUnicef::where('id', $id)->update([
                'img_path' => 'img/unicef_img/riset_'.$id.'.'.$request->image->getClientOriginalExtension(),
                'content' => $request->desc
            ]);
        }else{
            RisetUnicef::where('id', $id)->update([
                'content' => $request->desc
            ]);
        }


        return redirect('/disagree');
    }
}
