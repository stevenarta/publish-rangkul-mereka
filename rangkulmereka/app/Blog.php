<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'title','subtitle','category','desc','url','image','postedBy','views'
    ];
}
