<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalKekerasan extends Model
{
    protected $fillable = [
        'total'
    ];
}
