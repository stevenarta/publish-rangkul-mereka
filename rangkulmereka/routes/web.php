<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Blog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::middleware('isUser')->group(function(){

});

Route::middleware('isAdmin')->group(function(){

});

// ====================HOME==================
Route::get('/home', 'HomeController@index' );
Route::get('/', 'HomeController@index' );

Route::middleware('isAdmin')->group(function(){
    Route::get('/total-kekerasan/edit', 'HomeController@totalKekerasanEdit' );
    Route::post('/total-kekerasan/edit', 'HomeController@totalKekerasanUpdate' );

    Route::get('/carousel/create', 'HomeController@carouselCreate' );
    Route::post('/carousel/create', 'HomeController@carouselStore' );
    Route::get('/carousel/edit', 'HomeController@carouselEdit' );
    Route::post('/carousel/edit', 'HomeController@carouselUpdate' );
    Route::get('/carousel/delete', 'HomeController@carouselDelete' );
    Route::post('/carousel/delete', 'HomeController@carouselDestroy' );
});

// ===================BLOG==================
Route::get('/blog', 'BlogController@index' );
Route::get('/blog/show/{id}', 'BlogController@show' );

Route::middleware('isAdmin')->group(function(){
    Route::get('/blog/create', 'BlogController@create' );
    Route::post('/blog/create', 'BlogController@store' );
    Route::get('/blog/edit/{id}', 'BlogController@edit' );
    Route::post('/blog/edit/{id}', 'BlogController@update' );
    Route::get('/blog/delete/{id}', 'BlogController@destroy' );
});

// Route::get('/blog_image/fetch_image/{id}', 'BlogController@fetch_image' );

// -----------------DATA API----------------

Route::get('/api/blog/{category?}', 'DataApiController@getBlog' );
Route::get('/api/dampak', 'DataApiController@getDampak' );
Route::get('/api/stop-kekerasan', 'DataApiController@getStopKekerasan' );
Route::get('/api/quiz', 'DataApiController@getQuiz' );
Route::get('/api/carousel', 'DataApiController@getCarousel' );
Route::get('/api/user-score/create/{score}', 'DataApiController@insertUserScore');


// =========================================

// ========================Disagree===================
Route::get('/disagree/{page}', 'DisagreeController@index');

Route::middleware('isAdmin')->group(function(){
    Route::get('/laporan/edit', 'DisagreeController@laporanEdit' );
    Route::post('/laporan/edit', 'DisagreeController@laporanUpdate' );
    Route::get('/riset/edit/{id}', 'DisagreeController@risetEdit' );
    Route::post('/riset/edit/{id}', 'DisagreeController@risetUpdate' );
});


// ============================About===================
Route::get('/about', 'AboutController@index' );

Route::middleware('isAdmin')->group(function(){
    Route::get('/ig/add', 'AboutController@IgCreate' );
    Route::post('/ig/add', 'AboutController@IgStore' );
    Route::get('/ig/edit/{id}', 'AboutController@IgEdit' );
    Route::post('/ig/edit/{id}', 'AboutController@IgUpdate' );
    Route::get('/ig/delete/{id}', 'AboutController@IgDestroy' );
});


//================================Support======================
Route::get('/support', 'SupportController@index' );

Route::middleware('isAdmin')->group(function(){
    Route::get('/kekerasan/edit/{id}', 'SupportController@kekerasanEdit' );
    Route::post('/kekerasan/edit/{id}', 'SupportController@kekerasanUpdate' );

    Route::get('/dampak/add', 'SupportController@dampakCreate' );
    Route::post('/dampak/add', 'SupportController@dampakStore' );
    Route::get('/dampak/edit/{id}', 'SupportController@dampakEdit' );
    Route::post('/dampak/edit/{id}', 'SupportController@dampakUpdate' );
    Route::get('/dampak/delete/{id}', 'SupportController@dampakDestroy' );

    Route::get('/stop-kekerasan/add', 'SupportController@stopCreate' );
    Route::post('/stop-kekerasan/add', 'SupportController@stopStore' );
    Route::get('/stop-kekerasan/edit/{id}', 'SupportController@stopEdit' );
    Route::post('/stop-kekerasan/edit/{id}', 'SupportController@stopUpdate' );
    Route::get('/stop-kekerasan/delete/{id}', 'SupportController@stopDestroy' );
});


// =========================Quiz===========================
Route::middleware('isAuth')->group(function(){
    Route::get('/quiz', 'QuizController@index');
});

Route::get('/result/lvl1', function(){
    return view('QuizResult.level1');
});

Route::get('/result/lvl2', function(){
    return view('QuizResult.level2');
});

Route::get('/result/malaikat', function(){
    return view('QuizResult.malaikat');
});

Route::middleware('isAdmin')->group(function(){
    Route::get('/admin/quiz', 'AdminQuizController@index');
    Route::get('/admin/quiz/add', 'AdminQuizController@create' );
    Route::post('/admin/quiz/add', 'AdminQuizController@store' );
    Route::get('/admin/quiz/edit/{id}', 'AdminQuizController@edit' );
    Route::post('/admin/quiz/edit/{id}', 'AdminQuizController@update' );
    Route::get('/admin/quiz/delete/{id}', 'AdminQuizController@destroy' );
});

// =========================User Email===========================
Route::post('/email/add', 'UserEmailController@update' );


//Socialite Login
Route::middleware('isNotAuth')->group(function(){
    Route::get('login/{provider}','Auth\LoginController@redirectToProvider');
});

Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');



